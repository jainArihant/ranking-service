package batch

import (
	"errors"
	"fmt"
	"time"

	"bitbucket.org/swigy/ranking-service/internal/pkg/metrics"

	"bitbucket.org/swigy/ranking-service/api/rank"

	rankingservicev1 "bitbucket.org/swigy/protorepo/storefront/ranking-service/v1"
	"bitbucket.org/swigy/ranking-service/internal/handlers"
	"bitbucket.org/swigy/ranking-service/internal/utils"
	"github.com/rs/zerolog/log"
)

type BatchRankingService interface {
	BatchRank(request *rankingservicev1.BatchRankRequest) (*rankingservicev1.BatchRankResponse, error)
}

type BatchRankingHandler struct {
	RankingHandler handlers.RankingService
	PipelineConfig map[rankingservicev1.RankingEntity]PipelineConfig
}

const (
	DEFAULT_BATCH_SIZE          = 100
	DEFAULT_PIPELINE_TIMEOUT_MS = 1000
)

type PipelineConfig struct {
	TimeoutMs int64
	BatchSize int
}

func (c PipelineConfig) getBatchSize() int {
	if c.BatchSize == 0 {
		return DEFAULT_BATCH_SIZE
	}
	return c.BatchSize
}

func (c PipelineConfig) getTimeoutMs() int64 {
	if c.TimeoutMs == 0 {
		return DEFAULT_PIPELINE_TIMEOUT_MS
	}
	return c.TimeoutMs
}

func (rh *BatchRankingHandler) BatchRank(request *rankingservicev1.BatchRankRequest) (*rankingservicev1.BatchRankResponse, error) {
	log.Debug().Interface("batch_ranking_request", request).Msgf("Batch Ranking Request")

	err := rh.validateRequest(request)
	if err != nil {
		return nil, err
	}

	batchRankResponse := rankingservicev1.BatchRankResponse{
		Results: map[string]*rankingservicev1.RankingResponse{},
	}

	batchRequests := createRankRequests(request)
	batchReqLen := len(batchRequests)
	rankChannelResponses := rh.executeBatchRankAPI(batchReqLen, batchRequests)
	timeoutMs := rh.PipelineConfig[request.RankingEntity].getTimeoutMs()
	rankResponses := extractResponse(rankChannelResponses, batchReqLen, timeoutMs)

	var errs []error
	for _, rankResp := range rankResponses {
		if rankResp.Err != nil {
			errs = append(errs, rankResp.Err)
			continue
		}
		batchRankResponse.Results[rankResp.EntityGroupId] = rankResp.Response
	}

	if len(errs) > 0 && len(batchRankResponse.Results) == 0 {
		err := utils.CombineErrs(errs)
		log.Err(err).Msgf("Error occurred in batch rank request")
		return nil, err
	}
	return &batchRankResponse, nil
}

func (rh *BatchRankingHandler) validateRequest(request *rankingservicev1.BatchRankRequest) error {
	pipelineConfig := rh.PipelineConfig[request.RankingEntity]
	batchSize := pipelineConfig.getBatchSize()
	if request == nil {
		return errors.New("request cannot be nil")
	}
	if len(request.RankRequests) > batchSize {
		log.Error().Msgf("requests size cannot be greater than %v", batchSize)
		return errors.New(fmt.Sprintf("requests size cannot be greater than %v", batchSize))
	}
	return nil
}

func (rh *BatchRankingHandler) executeBatchRankAPI(batchReqLen int, batchRequests map[string]*rankingservicev1.RankingRequest) chan *rank.BatchRankResponse {
	rankChannel := make(chan *rank.BatchRankResponse, batchReqLen)
	for entityGroupId, req := range batchRequests {
		go rh.executeRankAPI(entityGroupId, req, rankChannel)
	}
	return rankChannel
}

func (rh *BatchRankingHandler) executeRankAPI(entityGroupId string, req *rankingservicev1.RankingRequest, channel chan *rank.BatchRankResponse) {
	rankResp, err := rh.RankingHandler.Rank(req)
	if err != nil {
		channel <- &rank.BatchRankResponse{
			EntityGroupId: entityGroupId,
			Response:      nil,
			Err:           err,
		}
		return
	}

	channel <- &rank.BatchRankResponse{
		EntityGroupId: entityGroupId,
		Response:      rankResp,
		Err:           nil,
	}
}

func createRankRequests(request *rankingservicev1.BatchRankRequest) map[string]*rankingservicev1.RankingRequest {
	batchRequests := map[string]*rankingservicev1.RankingRequest{}
	// Creating a map for ranking req uid to request

	for _, rankRequest := range request.RankRequests {
		rankingRequest := &rankingservicev1.RankingRequest{
			RequestCtx:       request.RequestCtx,
			UserClientInfo:   request.UserClientInfo,
			RankingEntity:    request.RankingEntity,
			EntityContextMap: rankRequest.EntityContextMap,
			ScoreProvider:    rankRequest.ScoreProvider,
		}
		batchRequests[rankRequest.EntityGroupId] = rankingRequest
	}
	return batchRequests
}

func extractResponse(channel chan *rank.BatchRankResponse, reqLen int, timeoutMs int64) []*rank.BatchRankResponse {

	var batchRankResponse []*rank.BatchRankResponse
	timeout := time.After(time.Duration(timeoutMs) * time.Millisecond)
	for i := 0; i < reqLen; i++ {
		select {
		case rankResp := <-channel:
			batchRankResponse = append(batchRankResponse, rankResp)
		case <-timeout:
			log.Error().Msgf("%v out of %v requests have been processed", len(batchRankResponse), reqLen)
			metrics.GetPrometheusInstance().GenericCounter().WithLabelValues("batch-rank-timeout").Inc()
			return batchRankResponse
		}
	}
	return batchRankResponse
}

package handlers

import (
	"fmt"
	"reflect"
	"testing"
	"time"

	marketplacev1 "bitbucket.org/swigy/protorepo/platform/bundle-commons/marketplace/v1"
	rankingservicev1 "bitbucket.org/swigy/protorepo/storefront/ranking-service/v1"
	sharedv1 "bitbucket.org/swigy/protorepo/storefront/storefront-shared/v1"
	"bitbucket.org/swigy/ranking-service/internal/pipeline"
	"bitbucket.org/swigy/ranking-service/internal/pkg/configs"
	"bitbucket.org/swigy/ranking-service/mocks"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/mock"
	"google.golang.org/genproto/googleapis/type/latlng"
)

func Test_Rank(t *testing.T) {
	type fields struct {
		Config     *configs.Config
		ServiceMap map[rankingservicev1.RankingEntity]pipeline.Constructor
	}
	type args struct {
		request *rankingservicev1.RankingRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *rankingservicev1.RankingResponse
		wantErr bool
	}{
		{
			name: "first_success_test",
			fields: fields{
				Config: configs.GetConfiguration(),
				ServiceMap: map[rankingservicev1.RankingEntity]pipeline.Constructor{
					rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM: func(*configs.Config, *rankingservicev1.RankingRequest) pipeline.RankingPipeline {
						return &mocks.RankingPipeline{
							Mock: mock.Mock{
								ExpectedCalls: []*mock.Call{
									{
										Method: "ValidateRequest",
										Arguments: mock.Arguments{
											&rankingservicev1.RankingRequest{
												RequestCtx:       getRankingRequest().RequestCtx,
												UserClientInfo:   getRankingRequest().UserClientInfo,
												RankingEntity:    getRankingRequest().RankingEntity,
												EntityContextMap: getRankingRequest().EntityContextMap,
												ScoreProvider:    getRankingRequest().ScoreProvider,
											},
										},
										ReturnArguments: mock.Arguments{
											nil,
										},
									},
									{
										Method:    "BuildContext",
										Arguments: mock.Arguments{},
										ReturnArguments: mock.Arguments{
											nil,
										},
									},
									{
										Method:    "CallExternal",
										Arguments: mock.Arguments{},
										ReturnArguments: mock.Arguments{
											nil,
										},
									},
									{
										Method:    "EnrichRankingObject",
										Arguments: mock.Arguments{},
										ReturnArguments: mock.Arguments{
											nil,
										},
									},
									{
										Method:    "FilterRequest",
										Arguments: mock.Arguments{},
										ReturnArguments: mock.Arguments{
											nil,
										},
									},
									{
										Method:    "ExecuteRanking",
										Arguments: mock.Arguments{},
										ReturnArguments: mock.Arguments{
											nil,
										},
									},
									{
										Method:    "GenerateResponse",
										Arguments: mock.Arguments{},
										ReturnArguments: mock.Arguments{
											&rankingservicev1.RankingResponse{
												RankingObjects: []*rankingservicev1.RankingResult{
													{
														EntityId: "123",
														Meta:     nil,
														Score:    1.5,
													},
												},
											},
											nil,
										},
									},
								},
								Calls: nil,
							},
						}
					},
				},
			},
			args: args{request: getRankingRequest()},
			want: &rankingservicev1.RankingResponse{
				RankingObjects: []*rankingservicev1.RankingResult{
					{
						EntityId: "123",
						Score:    1.5,
					},
				},
			},
		},
		{
			name: "validate_request_failed",
			fields: fields{
				Config: configs.GetConfiguration(),
				ServiceMap: map[rankingservicev1.RankingEntity]pipeline.Constructor{
					rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM: func(*configs.Config, *rankingservicev1.RankingRequest) pipeline.RankingPipeline {
						return &mocks.RankingPipeline{
							Mock: mock.Mock{
								ExpectedCalls: []*mock.Call{
									{
										Method: "ValidateRequest",
										Arguments: mock.Arguments{
											&rankingservicev1.RankingRequest{
												RequestCtx:       getRankingRequest().RequestCtx,
												UserClientInfo:   getRankingRequest().UserClientInfo,
												RankingEntity:    getRankingRequest().RankingEntity,
												EntityContextMap: getRankingRequest().EntityContextMap,
												ScoreProvider:    getRankingRequest().ScoreProvider,
											},
										},
										ReturnArguments: mock.Arguments{
											fmt.Errorf("validation failed"),
										},
									},
								},
								Calls: nil,
							},
						}
					},
				},
			},
			args:    args{request: getRankingRequest()},
			want:    nil,
			wantErr: true,
		},
		{
			name: "service_not_register",
			fields: fields{
				Config: configs.GetConfiguration(),
				ServiceMap: map[rankingservicev1.RankingEntity]pipeline.Constructor{
					rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM: func(*configs.Config, *rankingservicev1.RankingRequest) pipeline.RankingPipeline {
						return &mocks.RankingPipeline{
							Mock: mock.Mock{
								ExpectedCalls: []*mock.Call{
									{},
								},
								Calls: nil,
							},
						}
					},
				},
			},
			args:    args{request: getRankingRequestForNotRegisterService()},
			want:    nil,
			wantErr: true,
		},{
			name:"panic_handle",
			fields: fields{
				Config:     nil,
				ServiceMap: nil,
			},
			args: args{
				request: nil,
			},
			want: nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			handler := &RankingHandler{
				Config:     tt.fields.Config,
				ServiceMap: tt.fields.ServiceMap,
			}
			got, err := handler.Rank(tt.args.request)
			if !reflect.DeepEqual(got, tt.want) || (err != nil && !tt.wantErr) {
				t.Errorf("failed %v", tt.name)
				log.Error().Msgf("failed %v got %v want %v", tt.name, got, tt.want)
			}
		})
	}
}

func getRankingRequest() *rankingservicev1.RankingRequest {
	return &rankingservicev1.RankingRequest{
		RequestCtx: &marketplacev1.RequestContext{
			MarketPlaceId:  0,
			BusinessLineId: 0,
			CategoryId:     0,
			RequestTime:    nil,
			LatLng:         &latlng.LatLng{Latitude: 123.0, Longitude: 123.0},
		},
		UserClientInfo: &sharedv1.UserClientInfo{
			DeviceId:         "abcd",
			Swuid:            "",
			Tid:              "",
			Ip:               "",
			Sid:              "",
			UserAgent:        "",
			UserAgentVersion: "",
			Channel:          "",
			UserId:           "12345",
		},
		RankingEntity:    rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM,
		EntityContextMap: map[string]*any.Any{"123": entityInfoInAny()},
		ScoreProvider:    "",
	}
}

func getRankingRequestForNotRegisterService() *rankingservicev1.RankingRequest {
	return &rankingservicev1.RankingRequest{
		RankingEntity: 100,
	}
}

func entityInfoInAny() *any.Any {
	b, _ := ptypes.MarshalAny(&rankingservicev1.EntitySortingInfo{
		RankingEntityInfo: []*rankingservicev1.RankingEntityInfo{{
			Id:           "",
			StoreId:      "",
			Orderability: false,
		}},
		SortingInfo: &rankingservicev1.SortingInfo{
			NodeId:     "123",
			TaxonomyId: "123",
			Time: &timestamp.Timestamp{
				Seconds: int64(time.Now().Second()),
				Nanos:   0,
			},
			ListingSlot: "",
		},
	})
	return b
}

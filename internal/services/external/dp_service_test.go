package external

import (
	"bitbucket.org/swigy/ranking-service/internal/pkg/configs"
	"testing"
	"time"
)

func TestDPServiceImpl_CallDpEvent(t *testing.T) {
	type fields struct {
		Config *configs.Config
	}
	type args struct {
		request *OOSIMProductDPEvent
	}
	tests := []struct{
		name string
		fields fields
		args args
	}{
		{
			name: "dp_event_call",
			fields: fields{
				Config: &configs.Config{
					AppConfig:             configs.AppConfig{},
					LogConfig:             configs.LogConfig{},
					ExternalServiceConfig: configs.ExternalServiceConfig{
						DPHost: "localhost:8000",
						DPApplicationName: "dpAppName",
						DPOOSProductEventName: "oosProductEvent",
						DPOOSProductEventSchemaVersion: "ooSchemaVersion",
					},
				},
			},
			args : args{
				request: &OOSIMProductDPEvent{
					Sid:             "sid",
					Tid:             "tid",
					DeviceId:        "deviceId",
					UserId:          "userId",
					EntityScoreList: []*EntityScore{},
					CreatedAt:       time.Now().Format("2006-01-02 15:04:05"),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dpCaller := &DPServiceImpl{
				Config: tt.fields.Config,
			}
			dpCaller.CallDpEvent(tt.args.request)
		})
	}
}

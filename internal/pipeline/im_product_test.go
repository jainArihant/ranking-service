package pipeline

import (
	marketplacev1 "bitbucket.org/swigy/protorepo/platform/bundle-commons/marketplace/v1"
	rankingservicev1 "bitbucket.org/swigy/protorepo/storefront/ranking-service/v1"
	sharedv1 "bitbucket.org/swigy/protorepo/storefront/storefront-shared/v1"
	"bitbucket.org/swigy/ranking-service/internal/pipeline/services/context"
	"bitbucket.org/swigy/ranking-service/internal/pkg/circuitbreaker"
	conf "bitbucket.org/swigy/ranking-service/internal/pkg/configs"
	"bitbucket.org/swigy/ranking-service/internal/services/external"
	"bitbucket.org/swigy/ranking-service/mocks"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"
	"github.com/golang/protobuf/ptypes/timestamp"
	"google.golang.org/genproto/googleapis/type/latlng"
	"reflect"
	"testing"
	"time"
)

func TestIMProductPipeline_ValidateRequest(t *testing.T) {
	type args struct {
		request *rankingservicev1.RankingRequest
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "user_id_present_device_id_absent",
			args: args{
				request: &rankingservicev1.RankingRequest{
					UserClientInfo: &sharedv1.UserClientInfo{
						DeviceId: "",
						UserId:   "123",
					},
					RequestCtx: &marketplacev1.RequestContext{
						LatLng: &latlng.LatLng{
							Latitude:  2.3,
							Longitude: 2.3,
						},
					},
				},
			},
			wantErr: false,
		},
		{
			name: "user_id_absent_device_id_present",
			args: args{
				request: &rankingservicev1.RankingRequest{
					UserClientInfo: &sharedv1.UserClientInfo{
						DeviceId: "abc",
						UserId:   "",
					},
					RequestCtx: &marketplacev1.RequestContext{
						LatLng: &latlng.LatLng{
							Latitude:  2.3,
							Longitude: 2.3,
						},
					},
				},
			},
			wantErr: false,
		},
		{
			name: "user_id_and_device_id_absent",
			args: args{
				request: &rankingservicev1.RankingRequest{
					UserClientInfo: &sharedv1.UserClientInfo{
						DeviceId: "",
						UserId:   "",
					},
					RequestCtx: &marketplacev1.RequestContext{
						LatLng: &latlng.LatLng{
							Latitude:  2.3,
							Longitude: 2.3,
						},
					},
				},
			},
			wantErr: true,
		},
		{
			name: "latlong_not_present",
			args: args{
				request: &rankingservicev1.RankingRequest{
					UserClientInfo: &sharedv1.UserClientInfo{
						DeviceId: "abc",
						UserId:   "123",
					},
					RequestCtx: &marketplacev1.RequestContext{
						LatLng: nil,
					},
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		improduct := &IMProductPipeline{
		}
		err := improduct.ValidateRequest(tt.args.request)
		if err != nil && !tt.wantErr {
			t.Errorf("failed %v with err %v", tt.name, err)
		}
	}
}

func TestIMProductPipeline_BuildContext(t *testing.T) {
	type fields struct {
		request *rankingservicev1.RankingRequest
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "valid_request",
			fields: fields{request: &rankingservicev1.RankingRequest{
				RequestCtx:       nil,
				UserClientInfo:   nil,
				RankingEntity:    0,
				EntityContextMap: nil,
				ScoreProvider:    "",
			}},
			wantErr: false,
		},
		{
			name:    "request is nil",
			fields:  fields{request: nil},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		improduct := &IMProductPipeline{
			Request: tt.fields.request,
		}
		err := improduct.BuildContext()
		if err != nil && !tt.wantErr {
			t.Errorf("failed %v with error %v", tt.name, err)
		}
	}

}

func TestIMProductPipeline_CallExternal(t *testing.T) {
	tests := []struct {
		name    string
		wantErr bool
	}{
		{
			name:    "blank_call",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		improduct := &IMProductPipeline{
		}
		improduct.CallExternal()
		if tt.wantErr {

		}
	}
}

func TestIMProductPipeline_EnrichRankingObject(t *testing.T) {
	type fields struct {
		Context          *context.PipelineCtx
		ProductEntityMap map[string]*ProductEntity
	}
	tests := []struct {
		name   string
		fields fields
		want   map[string]*ProductEntity
	}{
		{
			name: "Successful_Enrich",
			fields: fields{
				Context: &context.PipelineCtx{
					UserId:   "",
					GeoHash:  "",
					CityId:   "",
					DeviceId: "",
					SwuId:    "",
					Tid:      "",
					Sid:      "",
					EntityObjectMap: map[string]*any.Any{
						"123": entityInfoInAny(),
					},
					ScoreProvider: "",
				},
				ProductEntityMap: map[string]*ProductEntity{
					"123": {
						ProductId:    "",
						TaxonomyId:   "",
						NodeId:       "",
						Score:        0,
						StoreId:      "",
						Orderability: false,
						Time:         time.Unix(100000000, 0),
					},
				},
			},
			want: map[string]*ProductEntity{
				"123": {
					ProductId:    "123",
					TaxonomyId:   "123",
					NodeId:       "123",
					Score:        0,
					StoreId:      "",
					Orderability: false,
					Time:         time.Unix(100000000, 0),
				},
			},
		},
		{
			name: "parse_error",
			fields: fields{
				Context: &context.PipelineCtx{
					UserId:   "",
					GeoHash:  "",
					Time:     nil,
					CityId:   "",
					DeviceId: "",
					SwuId:    "",
					Tid:      "",
					Sid:      "",
					EntityObjectMap: map[string]*any.Any{
						"123": nil,
					},
					ScoreProvider: "",
				},
				ProductEntityMap: nil,
			},
			want: map[string]*ProductEntity{
			},
		},
	}
	for _, tt := range tests {
		improduct := &IMProductPipeline{
			Context:          tt.fields.Context,
			ProductEntityMap: tt.fields.ProductEntityMap,
		}
		improduct.EnrichRankingObject()
		if !reflect.DeepEqual(improduct.ProductEntityMap, tt.want) {
			t.Errorf("failed %v got %v want %v", tt.name, improduct.ProductEntityMap, tt.want)
		}
	}
}

func TestIMProductPipeline_FilterRequest(t *testing.T) {
	type fields struct {
		Config    *conf.Config
		EntityMap map[string]*ProductEntity
	}
	tests := []struct {
		name   string
		fields fields
		want   map[string]float64
	}{
		{
			name: "filter_not_in_stock",
			fields: fields{
				Config: &conf.Config{
					BusinessConfig: conf.BusinessConfig{
						IMStores:             "",
						DefaultScoreInStock:  0,
						DefaultScoreOutStock: -1,
					},
				},
				EntityMap: map[string]*ProductEntity{
					"123": {
						ProductId:    "123",
						TaxonomyId:   "123",
						NodeId:       "123",
						Score:        0,
						StoreId:      "",
						Orderability: false,
						Time:         time.Now(),
					},
				},
			},
			want: map[string]float64{
				"123": -1,
			},
		},
		{
			name: "filter_in_stock_not_im_stores",
			fields: fields{
				Config: &conf.Config{
					BusinessConfig: conf.BusinessConfig{
						IMStores:             "123",
						DefaultScoreInStock:  0,
						DefaultScoreOutStock: -1,
					},
				},
				EntityMap: map[string]*ProductEntity{
					"123": {
						ProductId:    "123",
						TaxonomyId:   "123",
						NodeId:       "123",
						Score:        0,
						StoreId:      "122",
						Orderability: true,
						Time:         time.Now(),
					},
				},
			},
			want: map[string]float64{
				"123": 0,
			},
		},
		{
			name: "not_filter_in_stock_im_stores",
			fields: fields{
				Config: &conf.Config{
					BusinessConfig: conf.BusinessConfig{
						IMStores:             "123",
						DefaultScoreInStock:  0,
						DefaultScoreOutStock: -1,
					},
				},
				EntityMap: map[string]*ProductEntity{
					"123": {
						ProductId:    "123",
						TaxonomyId:   "123",
						NodeId:       "123",
						Score:        0,
						StoreId:      "123",
						Orderability: true,
						Time:         time.Now(),
					},
				},
			},
			want: map[string]float64{},
		},
	}
	for _, tt := range tests {
		improduct := &IMProductPipeline{
			Config:           tt.fields.Config,
			ProductEntityMap: tt.fields.EntityMap,
		}
		improduct.FilterRequest()
		if !reflect.DeepEqual(tt.want, improduct.FilteredProductMap) {
			t.Errorf("%v failed got %v want %v", tt.name, improduct.FilteredProductMap, tt.want)
		}
	}

}

func TestIMProductPipeline_ExecuteRanking(t *testing.T) {
	asyncResp := make(chan *circuitbreaker.AsyncResponse, 1)
	asyncResp <- &circuitbreaker.AsyncResponse{
		Resp: nil,
		Err:  nil,
	}
	pipelinectx := &context.PipelineCtx{
		UserId:        "",
		GeoHash:       "",
		Time:          nil,
		CityId:        "",
		DeviceId:      "",
		SwuId:         "",
		Tid:           "",
		Sid:           "",
		ScoreProvider: "",
	}
	entityMap := map[string]*ProductEntity{
		"123": {
			ProductId:    "123",
			TaxonomyId:   "",
			NodeId:       "",
			Score:        0,
			StoreId:      "222",
			Orderability: false,
			Time:         time.Now(),
		},
	}
	dspService := new(mocks.DSPService)
	dpService := new(mocks.DPService)
	dspService.On("CallIMCategoryProduct", buildIMProductReq(pipelinectx, entityMap,
		nil)).Return(&external.AsynIMCategoryProductResp{IMCategoryProductChan: asyncResp})
	dspService.On("FetchIMCategoryProduct", &external.AsynIMCategoryProductResp{IMCategoryProductChan: asyncResp}).Return(
		&external.IMCategoryProductResp{Results: []*external.Result{{
			Id:        "123",
			ScoreInfo: nil,
		}},
			Status: 0,
		}, nil)
	sortedList := sortRankingEntity(entityMap)
	dpEventReqList := buildDPEventReqList(sortedList, pipelinectx)
	dpService.On("CallDpEvent", dpEventReqList)
	type fields struct {
		context   *context.PipelineCtx
		entityMap map[string]*ProductEntity
		filterMap map[string]float64
		dspCall   external.DSPService
		dpCall    external.DPService
	}
	tests := []struct {
		name   string
		fields fields
		want   *external.IMCategoryProductResp
	}{
		{
			name: "Success_DspCall",
			fields: fields{
				context:   pipelinectx,
				entityMap: entityMap,
				filterMap: nil,
				dspCall:   dspService,
				dpCall:    dpService,
			},
			want: &external.IMCategoryProductResp{
				Results: []*external.Result{{
					Id:        "123",
					ScoreInfo: nil,
				}},
				Status: 0,
			},
		},
	}
	for _, tt := range tests {
		improduct := &IMProductPipeline{
			Context:            tt.fields.context,
			ProductEntityMap:   tt.fields.entityMap,
			FilteredProductMap: tt.fields.filterMap,
			DspCaller:          tt.fields.dspCall,
			DpCaller:           tt.fields.dpCall,
		}
		improduct.ExecuteRanking()
		if !reflect.DeepEqual(tt.want, improduct.ImCategoryProductResp) {
			t.Errorf("failed %v got %v want %v", tt.name, improduct.ImCategoryProductResp, tt.want)
		}
	}
}

func TestIMProductPipeline_GenerateResponse(t *testing.T) {
	type fields struct {
		ImCategoryProductResp *external.IMCategoryProductResp
		FilterMap             map[string]float64
	}
	tests := []struct {
		name   string
		fields fields
		want   *rankingservicev1.RankingResponse
	}{
		{
			name: "Success_response",
			fields: fields{
				ImCategoryProductResp: &external.IMCategoryProductResp{
					Results: []*external.Result{
						{
							Id:        "222",
							ScoreInfo: &external.ScoreInfo{Score: 0.5},
						},
					},
					Status: 0,
				},
				FilterMap: map[string]float64{
					"123": -1,
				},
			},
			want: &rankingservicev1.RankingResponse{
				RankingObjects: []*rankingservicev1.RankingResult{
					{
						EntityId: "222",
						Meta:     nil,
						Score:    0.5,
					},
					{
						EntityId: "123",
						Meta:     nil,
						Score:    -1,
					},
				},
			},
		},
	}
	for _, tt := range tests {
		improduct := &IMProductPipeline{
			ImCategoryProductResp: tt.fields.ImCategoryProductResp,
			FilteredProductMap:    tt.fields.FilterMap,
		}
		got, _ := improduct.GenerateResponse()
		if !reflect.DeepEqual(got, tt.want) {
			t.Errorf("failed %v got %v want %v", tt.name, got, tt.want)
		}
	}
}
func entityInfoInAny() *any.Any {
	b, _ := ptypes.MarshalAny(&rankingservicev1.EntitySortingInfo{
		RankingEntityInfo: []*rankingservicev1.RankingEntityInfo{{
			Id:           "",
			StoreId:      "",
			Orderability: false,
		}},
		SortingInfo: &rankingservicev1.SortingInfo{
			NodeId:      "123",
			TaxonomyId:  "123",
			ListingSlot: "",
			Time:&timestamp.Timestamp{
				Seconds: 100000000,
				Nanos:   0,
			},
		},
	})
	return b
}

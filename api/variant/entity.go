package variant

import "google.golang.org/genproto/googleapis/type/money"

type Entity struct {
	EntityId        string
	VariantId       string
	StoreId         string
	Orderability    bool
	MrpPrice        *money.Money
	OfferPrice      *money.Money
	WeightValue     float64
	DiscountPercent float64
}

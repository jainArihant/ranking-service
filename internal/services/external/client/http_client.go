package client

import (
	"bitbucket.org/swigy/ranking-service/internal/pkg/circuitbreaker"
	"bitbucket.org/swigy/ranking-service/internal/pkg/configs"
	"sync"
	"time"
)

var (
	dspProductPredictHttpClient circuitbreaker.ResilientHttpClient
	dpProductPredictHttpClient  circuitbreaker.ResilientHttpClient
	onlyOnce                    sync.Once
)

func InitialiseClients() {
	onlyOnce.Do(func() {
		config := configs.GetConfiguration()
		dspConfig := config.ExternalServiceConfig
		dspProductPredictHttpClient = circuitbreaker.NewResilientHttpClient(
			circuitbreaker.NewResilienceConfig(dspConfig.IMCategoryProductTimeoutMS*time.Millisecond, 2,
				2*time.Millisecond, dspConfig.IMCategoryProductMaxConcurrentRequest,
				dspConfig.IMCategoryProductErrorThresholdPercentage,
				dspConfig.IMCategoryProductRequestVolumeThreshold, "DSP_PRODUCT_PREDICT_CIRCUIT"))
		dpProductPredictHttpClient = circuitbreaker.NewResilientHttpClient(
			circuitbreaker.NewResilienceConfig(dspConfig.DPOOSProductEventTimeoutMS*time.Millisecond, 2,
				2*time.Millisecond, dspConfig.DPOOSProductEventMaxConcurrentRequest,
				dspConfig.DPOOSProductEventErrorThresholdPercentage,
				dspConfig.DPOOSProductEventRequestVolumeThreshold, "DP_OOS_PRODUCT_EVENT"))
	})
}

func GetDspProductPredictHttpClient() *circuitbreaker.ResilientHttpClient {
	InitialiseClients()
	return &dspProductPredictHttpClient
}

func GetDPProductPredictHttpClient() *circuitbreaker.ResilientHttpClient {
	InitialiseClients()
	return &dpProductPredictHttpClient
}

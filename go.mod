module bitbucket.org/swigy/ranking-service

go 1.13

require (
	bitbucket.org/swigy/protorepo/platform/bundle-commons v0.0.0-20210330081747-336603a2ee7c
	bitbucket.org/swigy/protorepo/storefront/ranking-service v0.0.0-20210416110635-e87942065559
	bitbucket.org/swigy/protorepo/storefront/storefront-shared v0.0.0-20210311111320-5122593de807
	github.com/cep21/circuit/v3 v3.1.1
	github.com/etherlabsio/healthcheck v0.0.0-20191224061800-dd3d2fd8c3f6
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/gojektech/heimdall v5.0.2+incompatible
	github.com/gojektech/valkyrie v0.0.0-20190210220504-8f62c1e7ba45 // indirect
	github.com/golang/protobuf v1.5.2
	github.com/google/go-cmp v0.5.5
	github.com/google/uuid v1.1.2
	github.com/gopherjs/gopherjs v0.0.0-20200217142428-fce0ec30dd00 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.1
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/jarcoal/httpmock v1.0.6
	github.com/jiacai2050/prometrics v0.0.4
	github.com/jnewmano/grpc-json-proxy v0.0.2
	github.com/kr/text v0.2.0 // indirect
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	github.com/mmcloughlin/geohash v0.10.0
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/prometheus/client_golang v1.7.1
	github.com/prometheus/common v0.13.0 // indirect
	github.com/rs/zerolog v1.20.0
	github.com/sirupsen/logrus v1.6.0
	github.com/smartystreets/assertions v1.1.1 // indirect
	github.com/spf13/afero v1.3.4 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.7.1
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/stretchr/testify v1.6.1
	go.uber.org/multierr v1.6.0
	go.uber.org/zap v1.16.0
	golang.org/x/net v0.0.0-20210510120150-4163338589ed // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/genproto v0.0.0-20210427215850-f767ed18ee4d
	google.golang.org/grpc v1.36.1
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/ini.v1 v1.60.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)

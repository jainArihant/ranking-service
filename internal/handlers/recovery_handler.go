package handlers

import (
	"bitbucket.org/swigy/ranking-service/internal/pkg/metrics"
	log "github.com/sirupsen/logrus"
	"runtime/debug"
)

func RecoverFromPanic(methodName string) bool {

	if r := recover(); r != nil {
		log.Errorf("panic in method : %v, panic : %v, stack : %v", methodName, r, string(debug.Stack()))
		metrics.GetPrometheusInstance().PanicCounter().WithLabelValues(methodName).Inc()
		return true
	}
	return false
}

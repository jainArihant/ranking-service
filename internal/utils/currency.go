package utils

import (
	"errors"
	"math"

	"github.com/rs/zerolog/log"
	"google.golang.org/genproto/googleapis/type/money"
)

const (
	CURRENCY_INR = "INR"
)

func GetGoogleMoneyFormat(val float64) *money.Money {
	if val == 0 {
		return nil
	}
	unit, frac := math.Modf(val)
	return &money.Money{
		CurrencyCode: CURRENCY_INR,
		Units:        int64(unit),
		Nanos:        int32(math.Round(frac * math.Pow10(9))),
	}
}

func FloatValueFromGoogleMoneyFormat(money *money.Money) float64 {
	if err := validateMoneyProto(money); err != nil {
		log.Error().Err(err).Msg("Unable to convert google money to float value")
		return 0
	}
	var val float64
	if money != nil {
		val = float64(money.Units)
		val += float64(money.Nanos) * math.Pow10(-9)
	}
	return val
}

func validateMoneyProto(money *money.Money) error {
	if money != nil {
		if money.GetCurrencyCode() != CURRENCY_INR {
			return errors.New("INVALID CURRENCY CODE")
		}
		if money.GetUnits() > 0 && money.GetNanos() < 0 {
			return errors.New("INVALID MONEY PROTO - UNIT IS POSITIVE, NANOS MUST BE POSITIVE OR ZERO")
		}
		if money.GetUnits() < 0 && money.GetNanos() > 0 {
			return errors.New("INVALID MONEY PROTO - UNIT IS NEGATIVE, NANOS MUST BE NEGATIVE OR ZERO")
		}
	}
	return nil
}

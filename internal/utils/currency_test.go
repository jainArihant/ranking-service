package utils

import (
	"reflect"
	"testing"

	"google.golang.org/genproto/googleapis/type/money"
)

func TestGetGoogleMoneyFormat(t *testing.T) {

	type args struct {
		value float64
	}
	tests := []struct {
		name string
		args args
		want *money.Money
	}{
		{
			name: "Valid Money Test1",
			args: args{123.4567},
			want: &money.Money{
				CurrencyCode: CURRENCY_INR,
				Units:        123,
				Nanos:        456700000,
			},
		},
		{
			name: "Valid Money Test2",
			args: args{99.01},
			want: &money.Money{
				CurrencyCode: CURRENCY_INR,
				Units:        99,
				Nanos:        10000000,
			},
		},
		{
			name: "Invalid Money Test3",
			args: args{0},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetGoogleMoneyFormat(tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetGoogleMoneyFormat() got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFloatValueFromGoogleMoneyFormat(t *testing.T) {

	type args struct {
		value *money.Money
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "Valid Money Test1",
			want: 123.4567,
			args: args{&money.Money{
				CurrencyCode: CURRENCY_INR,
				Units:        123,
				Nanos:        456700000,
			}},
		},
		{
			name: "Valid Money Test2",
			want: 99.01,
			args: args{&money.Money{
				CurrencyCode: CURRENCY_INR,
				Units:        99,
				Nanos:        10000000,
			}},
		},
		{
			name: "Invalid Money Test3",
			want: 0,
			args: args{nil},
		},
		{
			name: "Invalid Money Currency Code Test4",
			want: 0,
			args: args{&money.Money{
				CurrencyCode: "Invalid",
				Units:        99,
				Nanos:        10000000,
			}},
		},
		{
			name: "Invalid Money Nanos Test5",
			want: 0,
			args: args{&money.Money{
				CurrencyCode: CURRENCY_INR,
				Units:        99,
				Nanos:        -10000000,
			}},
		},
		{
			name: "Invalid Money Nanos Test6",
			want: 0,
			args: args{&money.Money{
				CurrencyCode: CURRENCY_INR,
				Units:        -99,
				Nanos:        10000000,
			}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FloatValueFromGoogleMoneyFormat(tt.args.value); got != tt.want {
				t.Errorf("GetGoogleMoneyFormat() got %v, want %v", got, tt.want)
			}
		})
	}
}

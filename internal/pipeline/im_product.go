package pipeline

import (
	rankingservicev1 "bitbucket.org/swigy/protorepo/storefront/ranking-service/v1"
	"bitbucket.org/swigy/ranking-service/internal/pipeline/services/context"
	"bitbucket.org/swigy/ranking-service/internal/pipeline/services/executor"
	"bitbucket.org/swigy/ranking-service/internal/pkg/configs"
	"bitbucket.org/swigy/ranking-service/internal/pkg/metrics"
	"bitbucket.org/swigy/ranking-service/internal/services/external"
	"bitbucket.org/swigy/ranking-service/internal/utils"
	"fmt"
	"github.com/golang/protobuf/ptypes"
	"github.com/mmcloughlin/geohash"
	"github.com/rs/zerolog/log"
	"sort"
	"strconv"
	"strings"
	time2 "time"
)

const (
	DATE_LAYOUT = "2006-01-02 15:04:05"
)

type IMProductPipeline struct {
	Request               *rankingservicev1.RankingRequest
	Config                *configs.Config
	Context               *context.PipelineCtx
	PipelineResponse      *executor.PipelineRankingResponse
	DspCaller             external.DSPService
	DpCaller              external.DPService
	ImCategoryProductResp *external.IMCategoryProductResp
	ProductEntityMap      map[string]*ProductEntity
	FilteredProductMap    map[string]float64
}

type ProductEntity struct {
	ProductId    string
	TaxonomyId   string
	NodeId       string
	Score        float64
	StoreId      string
	Orderability bool
	Time         time2.Time
}

func (ppp *IMProductPipeline) ValidateRequest(request *rankingservicev1.RankingRequest) error {
	if request.GetUserClientInfo().GetUserId() == "" && request.GetUserClientInfo().GetDeviceId() == "" {
		return fmt.Errorf("userId and deviceId is not present")
	}
	if request.GetRequestCtx().GetLatLng() == nil {
		return fmt.Errorf("lat long is not present")
	}
	ppp.Request = request
	return nil
}

func (ppp *IMProductPipeline) BuildContext() error {
	request := ppp.Request
	if request == nil {
		return fmt.Errorf("request is nil")
	}
	ppp.Context = &context.PipelineCtx{
		Time:            request.GetRequestCtx().GetRequestTime(),
		UserId:          request.GetUserClientInfo().GetUserId(),
		CityId:          strconv.FormatInt(int64(request.GetRequestCtx().GetCityId()), 10),
		DeviceId:        request.GetUserClientInfo().GetDeviceId(),
		Tid:             request.GetUserClientInfo().GetTid(),
		Sid:             request.GetUserClientInfo().GetSid(),
		SwuId:           request.GetUserClientInfo().GetSwuid(),
		ScoreProvider:   request.GetScoreProvider(),
		EntityObjectMap: request.GetEntityContextMap(),
		GeoHash: geohash.Encode(request.GetRequestCtx().GetLatLng().GetLatitude(),
			request.GetRequestCtx().GetLatLng().GetLongitude()),
	}
	return nil
}

func (ppp *IMProductPipeline) CallExternal() {
	return
}

func (ppp *IMProductPipeline) EnrichRankingObject() {
	entitymap := make(map[string]*ProductEntity, 1)
	entityObjectMap := ppp.Context.EntityObjectMap
	for entityId, object := range entityObjectMap {
		var entitySortingInfo rankingservicev1.EntitySortingInfo
		err := ptypes.UnmarshalAny(object, &entitySortingInfo)
		if err != nil {
			log.Error().Msgf("error occurred while unmarshal entity sorting info for %v", entityId)
			continue
		}
		productEntity := ProductEntity{
			ProductId:  entityId,
			TaxonomyId: entitySortingInfo.GetSortingInfo().GetTaxonomyId(),
			NodeId:     entitySortingInfo.GetSortingInfo().GetNodeId(),
			Score:      entitySortingInfo.GetSortingInfo().GetScore(),
			Time:       utils.GetValidTime(entitySortingInfo.GetSortingInfo().GetTime()),
		}
		for _, rankingentity := range entitySortingInfo.RankingEntityInfo {
			productEntity.StoreId = rankingentity.StoreId
			if rankingentity.Orderability {
				//if any sku is orderable then product is orderable.
				productEntity.Orderability = true
				break
			}
		}
		entitymap[entityId] = &productEntity
	}
	ppp.ProductEntityMap = entitymap
	return
}

func (ppp *IMProductPipeline) FilterRequest() {
	filterMap := make(map[string]float64, 1)
	imstores := strings.Split(ppp.Config.BusinessConfig.IMStores, ",")
	for productId, object := range ppp.ProductEntityMap {
		if log.Debug().Enabled() {
			log.Debug().Interface("entity_object", object).Msgf("entity_object")
		}
		if !object.Orderability {
			filterMap[productId] = ppp.Config.BusinessConfig.DefaultScoreOutStock
		}
	}
	for productId, object := range ppp.ProductEntityMap {
		if !utils.Contains(imstores, object.StoreId) {
			//if store is not present in instamart stores
			if _, ok := filterMap[productId]; !ok {
				filterMap[productId] = ppp.Config.BusinessConfig.DefaultScoreInStock
			}
		}
		if object.Score > 0 {
			if _, ok := filterMap[productId]; !ok {
				filterMap[productId] = object.Score
			}
		}
	}
	ppp.FilteredProductMap = filterMap
}

func (ppp *IMProductPipeline) ExecuteRanking() {
	sortedList := sortRankingEntity(ppp.ProductEntityMap)
	dpEventReqList := buildDPEventReqList(sortedList, ppp.Context)
	if len(dpEventReqList.EntityScoreList) > 0 {
		ppp.DpCaller.CallDpEvent(dpEventReqList)
	}

	request := buildIMProductReq(ppp.Context, ppp.ProductEntityMap, ppp.FilteredProductMap)
	if len(request.RequestList) == 0 {
		ppp.ImCategoryProductResp = nil
		return
	}
	asyncResp := ppp.DspCaller.CallIMCategoryProduct(request)
	response, err := ppp.DspCaller.FetchIMCategoryProduct(asyncResp)
	if err != nil {
		log.Error().Msgf("Error occurred while getting dsp info %v", err)
	}
	ppp.ImCategoryProductResp = response
}

func (ppp *IMProductPipeline) GenerateResponse() (*rankingservicev1.RankingResponse, error) {
	var rankingResults []*rankingservicev1.RankingResult
	if ppp.ImCategoryProductResp != nil {
		for _, rankObj := range ppp.ImCategoryProductResp.Results {
			rankingResults = append(rankingResults, &rankingservicev1.RankingResult{
				EntityId: rankObj.Id,
				Score:    float64(rankObj.ScoreInfo.Score),
			})
		}
	}
	for key, value := range ppp.FilteredProductMap {
		rankingResults = append(rankingResults, &rankingservicev1.RankingResult{
			EntityId: key,
			Meta:     nil,
			Score:    value,
		})
	}
	return &rankingservicev1.RankingResponse{
		RankingObjects: rankingResults,
	}, nil
}

func buildIMProductReq(ctx *context.PipelineCtx, entitymap map[string]*ProductEntity,
	filterMap map[string]float64) *external.IMCategoryProductReq {
	var requestList []*external.IMCategoryProductInfo
	cityId, _ := strconv.ParseInt(ctx.CityId, 10, 64)
	for productId, object := range entitymap {
		if _, ok := filterMap[productId]; ok {
			continue
		}
		nodeId := object.NodeId
		validTime := object.Time
		taxonomyId := object.TaxonomyId
		innerReq := &external.IMCategoryProductInfo{
			Id: productId,
			Req: &external.Req{
				CustomerId:  ctx.UserId,
				ListingSlot: utils.GetListingSlot(validTime),
				StoreId:     object.StoreId,
				Sid:         ctx.Sid,
				DayOfWeek:   int(validTime.Weekday()),
				NodeId:      nodeId,
				TaxonomyId:  taxonomyId,
				ProductId:   productId,
				Geohash:     ctx.GeoHash,
				CityId:      int(cityId),
			},
		}
		requestList = append(requestList, innerReq)
	}

	return &external.IMCategoryProductReq{RequestList: requestList}
}

func sortRankingEntity(entityMap map[string]*ProductEntity) []*ProductEntity {
	productEntities := []*ProductEntity{}
	for _, entity := range entityMap {
		productEntities = append(productEntities, entity)
	}
	sort.Slice(productEntities[:], func(i, j int) bool {
		return productEntities[i].Score >= productEntities[j].Score
	})
	return productEntities
}

func buildDPEventReqList(productEntitylist []*ProductEntity, context *context.PipelineCtx) *external.OOSIMProductDPEvent {
	rank := 1
	eventReqList := []*external.EntityScore{}
	for _, productEntity := range productEntitylist {
		if !productEntity.Orderability && productEntity.Score > 0 {
			metrics.GetPrometheusInstance().MethodRequestCounter().WithLabelValues(strconv.FormatInt(int64(rank/10),
				10), "oos_counter").Inc()
			eventReqList = append(eventReqList, &external.EntityScore{
				EntityType: "IM_PRODUCT",
				EntityId:   productEntity.ProductId,
				Score:      productEntity.Score,
				Rank:       rank,
			})
		}
		rank = rank + 1
	}
	return &external.OOSIMProductDPEvent{
		Sid:             context.Sid,
		Tid:             context.Tid,
		DeviceId:        context.DeviceId,
		UserId:          context.UserId,
		EntityScoreList: eventReqList,
		CreatedAt:       time2.Now().Format(DATE_LAYOUT),
	}
}

func NewIMProductPipeline(config *configs.Config, request *rankingservicev1.RankingRequest) RankingPipeline {
	return &IMProductPipeline{
		Config:    config,
		DspCaller: &external.DSPCaller{Config: config},
		DpCaller:  &external.DPServiceImpl{Config: config},
		Request:   request,
	}
}

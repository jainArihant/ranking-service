package sort

import (
	"reflect"
	"testing"

	"bitbucket.org/swigy/ranking-service/internal/utils"
	"google.golang.org/genproto/googleapis/type/money"

	"bitbucket.org/swigy/ranking-service/api/variant"

	"github.com/google/go-cmp/cmp"
)

func Test_compareDiscountPercent(t *testing.T) {
	boolTrue := true
	boolFalse := false
	type args struct {
		ei *variant.Entity
		ej *variant.Entity
	}
	tests := []struct {
		name string
		args args
		want *bool
	}{
		{
			name: "ei discount > ej discount",
			args: args{
				ei: &variant.Entity{
					EntityId:        "",
					VariantId:       "",
					StoreId:         "",
					Orderability:    false,
					MrpPrice:        createMoneyRs(150),
					OfferPrice:      createMoneyRs(50),
					WeightValue:     0,
					DiscountPercent: 50,
				},
				ej: &variant.Entity{
					EntityId:        "",
					VariantId:       "",
					StoreId:         "",
					Orderability:    false,
					MrpPrice:        createMoneyRs(100),
					OfferPrice:      createMoneyRs(50),
					WeightValue:     0,
					DiscountPercent: 10,
				},
			},
			want: &boolFalse,
		},
		{
			name: "ei discount < ej discount",
			args: args{
				ei: &variant.Entity{
					EntityId:        "",
					VariantId:       "",
					StoreId:         "",
					Orderability:    false,
					MrpPrice:        createMoneyRs(100),
					OfferPrice:      createMoneyRs(50),
					WeightValue:     0,
					DiscountPercent: 10,
				},
				ej: &variant.Entity{
					EntityId:        "",
					VariantId:       "",
					StoreId:         "",
					Orderability:    false,
					MrpPrice:        createMoneyRs(150),
					OfferPrice:      createMoneyRs(50),
					WeightValue:     0,
					DiscountPercent: 50,
				},
			},
			want: &boolTrue,
		},
		{
			name: "ei discount = ej discount",
			args: args{
				ei: &variant.Entity{
					EntityId:        "",
					VariantId:       "",
					StoreId:         "",
					Orderability:    false,
					MrpPrice:        createMoneyRs(150),
					OfferPrice:      createMoneyRs(75),
					WeightValue:     0,
					DiscountPercent: 50,
				},
				ej: &variant.Entity{
					EntityId:        "",
					VariantId:       "",
					StoreId:         "",
					Orderability:    false,
					MrpPrice:        createMoneyRs(100),
					OfferPrice:      createMoneyRs(50),
					WeightValue:     0,
					DiscountPercent: 50,
				},
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := compareDiscountPercentLt(tt.args.ei, tt.args.ej); !cmp.Equal(got, tt.want) {
				t.Errorf("compareDiscountPercentLt() = %v, want %v", got, tt.want)
				t.Errorf("compareDiscountPercentLt() = %v", cmp.Diff(got, tt.want))
			}
		})
	}
}

func Test_compareGrammage(t *testing.T) {
	type args struct {
		ei *variant.Entity
		ej *variant.Entity
	}
	boolTrue := true
	boolFalse := false
	tests := []struct {
		name string
		args args
		want *bool
	}{
		{
			name: "ei weight > e2 weight",
			args: args{
				ei: &variant.Entity{
					EntityId:     "",
					VariantId:    "",
					StoreId:      "",
					Orderability: false,
					MrpPrice:     nil,
					OfferPrice:   nil,
					WeightValue:  1000,
				},
				ej: &variant.Entity{
					EntityId:     "",
					VariantId:    "",
					StoreId:      "",
					Orderability: false,
					MrpPrice:     nil,
					OfferPrice:   nil,
					WeightValue:  500,
				},
			},
			want: &boolFalse,
		},
		{
			name: "ei weight = e2 weight",
			args: args{
				ei: &variant.Entity{
					EntityId:     "",
					VariantId:    "",
					StoreId:      "",
					Orderability: false,
					MrpPrice:     nil,
					OfferPrice:   nil,
					WeightValue:  500,
				},
				ej: &variant.Entity{
					EntityId:     "",
					VariantId:    "",
					StoreId:      "",
					Orderability: false,
					MrpPrice:     nil,
					OfferPrice:   nil,
					WeightValue:  500,
				},
			},
			want: nil,
		},
		{
			name: "ei weight = e2 weight",
			args: args{
				ei: &variant.Entity{
					EntityId:     "",
					VariantId:    "",
					StoreId:      "",
					Orderability: false,
					MrpPrice:     nil,
					OfferPrice:   nil,
					WeightValue:  500,
				},
				ej: &variant.Entity{
					EntityId:     "",
					VariantId:    "",
					StoreId:      "",
					Orderability: false,
					MrpPrice:     nil,
					OfferPrice:   nil,
					WeightValue:  1500,
				},
			},
			want: &boolTrue,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := compareGrammageLt(tt.args.ei, tt.args.ej); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("compareGrammageLt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_compareOrderability(t *testing.T) {
	type args struct {
		ei *variant.Entity
		ej *variant.Entity
	}
	boolTrue := true
	boolFalse := false
	tests := []struct {
		name string
		args args
		want *bool
	}{
		{
			name: "both oderable",
			args: args{
				ei: &variant.Entity{
					EntityId:     "",
					VariantId:    "",
					StoreId:      "",
					Orderability: true,
					MrpPrice:     nil,
					OfferPrice:   nil,
					WeightValue:  0,
				},
				ej: &variant.Entity{
					EntityId:     "",
					VariantId:    "",
					StoreId:      "",
					Orderability: true,
					MrpPrice:     nil,
					OfferPrice:   nil,
					WeightValue:  0,
				},
			},
			want: nil,
		},
		{
			name: "ei oderable",
			args: args{
				ei: &variant.Entity{
					EntityId:     "",
					VariantId:    "",
					StoreId:      "",
					Orderability: true,
					MrpPrice:     nil,
					OfferPrice:   nil,
					WeightValue:  0,
				},
				ej: &variant.Entity{
					EntityId:     "",
					VariantId:    "",
					StoreId:      "",
					Orderability: false,
					MrpPrice:     nil,
					OfferPrice:   nil,
					WeightValue:  0,
				},
			},
			want: &boolFalse,
		},
		{
			name: "ej oderable",
			args: args{
				ei: &variant.Entity{
					EntityId:     "",
					VariantId:    "",
					StoreId:      "",
					Orderability: false,
					MrpPrice:     nil,
					OfferPrice:   nil,
					WeightValue:  0,
				},
				ej: &variant.Entity{
					EntityId:     "",
					VariantId:    "",
					StoreId:      "",
					Orderability: true,
					MrpPrice:     nil,
					OfferPrice:   nil,
					WeightValue:  0,
				},
			},
			want: &boolTrue,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := compareOrderabilityLt(tt.args.ei, tt.args.ej); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("compareOrderabilityLt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func createMoneyRs(i int64) *money.Money {
	return &money.Money{
		CurrencyCode: utils.CURRENCY_INR,
		Units:        i,
		Nanos:        0,
	}
}

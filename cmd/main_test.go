package main

import (
	"testing"
)

func Test_main(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping testing in short mode")
	}
	main()
}

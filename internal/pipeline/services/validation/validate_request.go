package validation

import (
	rankingservicev1 "bitbucket.org/swigy/protorepo/storefront/ranking-service/v1"
)

type RequestValidator interface{
	ValidateRequest(request *rankingservicev1.RankingRequest) error
}

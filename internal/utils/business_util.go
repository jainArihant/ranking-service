package utils

import (
	"time"

	"github.com/golang/protobuf/ptypes/timestamp"
	"go.uber.org/multierr"
)

func GetValidTime(timestamp *timestamp.Timestamp) time.Time {
	currtime := time.Now()
	if timestamp != nil {
		currtime = time.Unix(timestamp.GetSeconds(), int64(timestamp.GetNanos()))
	}
	return currtime
}
func GetListingSlot(timestamp time.Time) int {
	h, _, _ := timestamp.Clock()
	switch h {
	case 7, 8:
		return 0
	case 9, 10:
		return 1
	case 11, 12, 13, 14:
		return 2
	case 15, 16, 17:
		return 3
	case 18, 19, 20:
		return 4
	case 21, 22:
		return 5
	case 23:
		return 6
	case 0, 1, 2:
		return 6
	default:
		return 7
	}
}

func Contains(list []string, s string) bool {
	for _, l := range list {
		if l == s {
			return true
		}
	}
	return false
}

func CombineErrs(errs []error) error {
	var err error
	if len(errs) != 0 {
		err = multierr.Combine(errs...)
	}
	return err
}

func ComputePercent(change float64, base float64) float64 {
	return (base - change) / base * 100
}

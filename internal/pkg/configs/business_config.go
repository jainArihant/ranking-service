package configs

import (
	"github.com/spf13/viper"
)

type BusinessConfig struct {
	IMStores                  string
	DefaultScoreInStock       float64
	DefaultScoreOutStock      float64
	VariantRankBatchSize      int
	ProductRankBatchSize      int
	VariantRankBatchTimeoutMs int64
	ProductRankBatchTimeoutMs int64
}

func getBusinessConfig() BusinessConfig {
	return BusinessConfig{
		IMStores:                  viper.GetString("IM_STORES"),
		DefaultScoreInStock:       viper.GetFloat64("DEFAULT_SCORE_IN_STOCK"),
		DefaultScoreOutStock:      viper.GetFloat64("DEFAULT_SCORE_OUT_STOCK"),
		VariantRankBatchSize:      viper.GetInt("VARIANT_RANK_BATCH_SIZE"),
		ProductRankBatchSize:      viper.GetInt("PRODUCT_RANK_BATCH_SIZE"),
		VariantRankBatchTimeoutMs: viper.GetInt64("VARIANT_RANK_BATCH_TIMEOUT_MS"),
		ProductRankBatchTimeoutMs: viper.GetInt64("PRODUCT_RANK_BATCH_TIMEOUT_MS"),
	}
}

func setBusinessConfigDefault() {
	viper.SetDefault("IM_STORES", "28584,78798,103108,72714,73114,73903,74004,99290,112207,112209,167908,175684")
	viper.SetDefault("DEFAULT_SCORE_IN_STOCK", 0)
	viper.SetDefault("DEFAULT_SCORE_OUT_STOCK", -1)

	viper.SetDefault("PRODUCT_RANK_BATCH_SIZE", 50)
	viper.SetDefault("VARIANT_RANK_BATCH_SIZE", 100)

	viper.SetDefault("PRODUCT_RANK_BATCH_TIMEOUT_MS", 500)
	viper.SetDefault("VARIANT_RANK_BATCH_TIMEOUT_MS", 50)
}

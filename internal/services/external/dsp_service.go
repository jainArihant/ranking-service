package external

import (
	"bitbucket.org/swigy/ranking-service/internal/pkg/circuitbreaker"
	"bitbucket.org/swigy/ranking-service/internal/pkg/configs"
	"bitbucket.org/swigy/ranking-service/internal/services/external/client"
	"bytes"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	"net/http"
)

type IMCategoryProductInfo struct {
	Id  string `json:"id"`
	Req *Req   `json:"rq"`
}

type Req struct {
	CustomerId  string `json:"customer_id"`
	ListingSlot int    `json:"listing_slot"`
	StoreId     string `json:"store_id"`
	Sid         string `json:"sid"`
	DayOfWeek   int    `json:"day_of_week"`
	NodeId      string `json:"node_id"`
	TaxonomyId  string `json:"taxonomy_id"`
	ProductId   string `json:"product_id"`
	Geohash     string `json:"geohash"`
	CityId      int    `json:"city_id"`
}

type IMCategoryProductReq struct {
	RequestList []*IMCategoryProductInfo
}

type Result struct {
	Id        string     `json:"id"`
	ScoreInfo *ScoreInfo `json:"res"`
}

type ScoreInfo struct {
	Score float32 `json:"score"`
}

type IMCategoryProductResp struct {
	Results []*Result `json:"result"`
	Status  int       `json:"status"`
}

type DSPService interface {
	CallIMCategoryProduct(request *IMCategoryProductReq) *AsynIMCategoryProductResp
	FetchIMCategoryProduct(imCategoryProductResp *AsynIMCategoryProductResp) (response *IMCategoryProductResp, err error)
}

type DSPCaller struct {
	Config *configs.Config
}

type AsynIMCategoryProductResp struct {
	IMCategoryProductChan chan *circuitbreaker.AsyncResponse
}

func (dc *DSPCaller) CallIMCategoryProduct(request *IMCategoryProductReq) *AsynIMCategoryProductResp {
	imCategoryProductChan := make(chan *circuitbreaker.AsyncResponse, 1)
	resilienceClient := client.GetDspProductPredictHttpClient()
	url := dc.Config.DSPHost + dc.Config.IMCategoryProductURL
	log.Info().Msgf("url %v", url)
	log.Info().Interface("dsp_request", request.RequestList).Msgf("im_product request for dsp")
	requestBody, err := json.Marshal(request.RequestList)
	if err != nil {
		log.Error().Msgf("error in getting requestbody %v", err)
		imCategoryProductChan <- &circuitbreaker.AsyncResponse{
			Resp: nil,
			Err:  err,
		}
		return &AsynIMCategoryProductResp{IMCategoryProductChan: imCategoryProductChan}
	}
	headers := http.Header{
		"content-type": []string{"application/json"},
	}
	httpRequest, err := http.NewRequest("POST", url, bytes.NewBuffer(requestBody))
	if err != nil {
		log.Error().Msgf("error in getting httpRequest %v", err)
		imCategoryProductChan <- &circuitbreaker.AsyncResponse{
			Resp: nil,
			Err:  err,
		}
		return &AsynIMCategoryProductResp{IMCategoryProductChan: imCategoryProductChan}
	}
	httpRequest.Header = headers
	go resilienceClient.AsyncDo(httpRequest, imCategoryProductChan)
	return &AsynIMCategoryProductResp{
		IMCategoryProductChan: imCategoryProductChan,
	}
}

func (dc *DSPCaller) FetchIMCategoryProduct(asynIMCategoryProductResp *AsynIMCategoryProductResp) (response *IMCategoryProductResp, err error) {

	resp := <-asynIMCategoryProductResp.IMCategoryProductChan
	if resp.Err != nil {
		return nil, resp.Err
	}
	defer resp.Resp.Body.Close()
	respByte, err := ioutil.ReadAll(resp.Resp.Body)
	if err != nil {
		return nil, err
	}
	var predictProdResp *IMCategoryProductResp
	err = json.Unmarshal(respByte, &predictProdResp)
	if err != nil {
		return nil, err
	}
	log.Info().Interface("dsp_response", predictProdResp).Msgf("im_product response for dsp")
	return predictProdResp, nil
}

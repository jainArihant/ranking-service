package configs

import (
	"github.com/spf13/viper"
	"sync"
)

type Config struct {
	AppConfig
	LogConfig
	ExternalServiceConfig
	BusinessConfig
}

func InitConfig() *Config {
	viper.AutomaticEnv()
	setConfigDefaults()
	return &Config{
		AppConfig:             getAppConfig(),
		LogConfig:             getLogConfig(),
		ExternalServiceConfig: getExternalServiceConfig(),
		BusinessConfig:        getBusinessConfig(),
	}
}

func setConfigDefaults() {
	setAppConfigDefaults()
	setLogConfigDefaults()
	setExternalServiceConfigDefault()
	setBusinessConfigDefault()
}

var Configuration *Config
var configInit sync.Once

func GetConfiguration() *Config {
	configInit.Do(func() {
		Configuration = InitConfig()
	})
	return Configuration
}

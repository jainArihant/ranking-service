package variant

import (
	"reflect"
	"strings"
	"testing"

	"bitbucket.org/swigy/ranking-service/api/variant"

	"github.com/google/go-cmp/cmp/cmpopts"

	"github.com/golang/protobuf/proto"

	v1 "bitbucket.org/swigy/protorepo/platform/bundle-commons/marketplace/v1"
	rankingservicev1 "bitbucket.org/swigy/protorepo/storefront/ranking-service/v1"
	v11 "bitbucket.org/swigy/protorepo/storefront/storefront-shared/v1"
	"bitbucket.org/swigy/ranking-service/internal/pipeline/services/context"
	"bitbucket.org/swigy/ranking-service/internal/pipeline/services/executor"
	"bitbucket.org/swigy/ranking-service/internal/pkg/configs"
	"bitbucket.org/swigy/ranking-service/internal/utils"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/google/go-cmp/cmp"
	"google.golang.org/genproto/googleapis/type/latlng"
	"google.golang.org/genproto/googleapis/type/money"
)

func TestIMVariantPipeline_BuildContext(t *testing.T) {
	type fields struct {
		Request          *rankingservicev1.RankingRequest
		Config           *configs.Config
		Context          *context.PipelineCtx
		PipelineResponse *executor.PipelineRankingResponse
		VariantEntityMap map[string]*variant.Entity
		VariantScoreMap  map[string]float64
	}
	currentTime := timestamp.Timestamp{
		Seconds: 1234,
		Nanos:   0,
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
		want    *context.PipelineCtx
	}{
		{
			name: "positive",
			fields: fields{
				Request: generateRequestCtx(currentTime),
			},
			wantErr: false,
			want: &context.PipelineCtx{
				UserId:   "CS1",
				GeoHash:  "td5kyhs3u",
				Time:     &currentTime,
				CityId:   "1",
				DeviceId: "DeviceId",
				SwuId:    "Swuid",
				Tid:      "tid",
				Sid:      "sid",
				EntityObjectMap: map[string]*any.Any{
					"v1": createAnyProto(&rankingservicev1.RankingEntityInfo{
						Id:            "v1",
						StoreId:       "s1",
						Orderability:  true,
						OfferPrice:    createMoneyRs(100),
						MrpPrice:      createMoneyRs(150),
						WeightInGrams: 10,
					}),
				},
				ScoreProvider: "",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vp := &IMVariantPipeline{
				Request:          tt.fields.Request,
				Config:           tt.fields.Config,
				Context:          tt.fields.Context,
				PipelineResponse: tt.fields.PipelineResponse,
				VariantEntityMap: tt.fields.VariantEntityMap,
				VariantScoreMap:  tt.fields.VariantScoreMap,
			}
			if err := vp.BuildContext(); (err != nil) != tt.wantErr {
				t.Errorf("BuildContext() error = %v, wantErr %v", err, tt.wantErr)
			} else {
				if !reflect.DeepEqual(vp.Context, tt.want) {
					t.Errorf("BuildContext() = %v", cmp.Diff(vp.Context, tt.want,
						cmpopts.IgnoreUnexported(timestamp.Timestamp{}, context.PipelineCtx{}, any.Any{})))
				}
			}
		})
	}
}

func TestIMVariantPipeline_ExecuteRanking(t *testing.T) {
	type fields struct {
		Request          *rankingservicev1.RankingRequest
		Config           *configs.Config
		Context          *context.PipelineCtx
		PipelineResponse *executor.PipelineRankingResponse
		VariantEntityMap map[string]*variant.Entity
		VariantScoreMap  map[string]float64
	}
	tests := []struct {
		name   string
		fields fields
		want   map[string]float64
	}{
		{
			name: "positive",
			fields: fields{
				Request:          nil,
				Config:           nil,
				Context:          nil,
				PipelineResponse: nil,
				VariantEntityMap: map[string]*variant.Entity{
					"v1": {
						EntityId:        "v1",
						VariantId:       "1",
						StoreId:         "1",
						Orderability:    true,
						MrpPrice:        createMoneyRs(100),
						OfferPrice:      createMoneyRs(10),
						WeightValue:     100,
						DiscountPercent: 90,
					},
					"v2": {
						EntityId:        "v2",
						VariantId:       "2",
						StoreId:         "1",
						Orderability:    true,
						MrpPrice:        createMoneyRs(100),
						OfferPrice:      createMoneyRs(20),
						WeightValue:     100,
						DiscountPercent: 20,
					},
				},
				VariantScoreMap: nil,
			},
			want: map[string]float64{
				"v1": 2,
				"v2": 1,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vp := &IMVariantPipeline{
				Request:          tt.fields.Request,
				Config:           tt.fields.Config,
				Context:          tt.fields.Context,
				PipelineResponse: tt.fields.PipelineResponse,
				VariantEntityMap: tt.fields.VariantEntityMap,
				VariantScoreMap:  tt.fields.VariantScoreMap,
			}

			vp.ExecuteRanking()
			if !cmp.Equal(vp.VariantScoreMap, tt.want, cmpopts.SortMaps(func(i, j string) bool {
				if strings.Compare(i, j) > 0 {
					return true
				}
				return false
			})) {
				t.Errorf("Diff() = %v", cmp.Diff(vp.VariantScoreMap, tt.want))
			}
		})
	}
}

func TestIMVariantPipeline_GenerateResponse(t *testing.T) {
	type fields struct {
		Request          *rankingservicev1.RankingRequest
		Config           *configs.Config
		Context          *context.PipelineCtx
		PipelineResponse *executor.PipelineRankingResponse
		VariantEntityMap map[string]*variant.Entity
		VariantScoreMap  map[string]float64
	}
	tests := []struct {
		name    string
		fields  fields
		want    *rankingservicev1.RankingResponse
		wantErr bool
	}{
		{
			name: "positive",
			fields: fields{
				Request:          nil,
				Config:           nil,
				Context:          nil,
				PipelineResponse: nil,
				VariantEntityMap: nil,
				VariantScoreMap: map[string]float64{
					"v1": 1,
					"v2": 2,
					"v3": 3,
				},
			},
			want: &rankingservicev1.RankingResponse{
				RankingObjects: []*rankingservicev1.RankingResult{
					{
						EntityId: "v1",
						Meta:     nil,
						Score:    1,
					},
					{
						EntityId: "v2",
						Meta:     nil,
						Score:    2,
					},
					{
						EntityId: "v3",
						Meta:     nil,
						Score:    3,
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vp := &IMVariantPipeline{
				Request:          tt.fields.Request,
				Config:           tt.fields.Config,
				Context:          tt.fields.Context,
				PipelineResponse: tt.fields.PipelineResponse,
				VariantEntityMap: tt.fields.VariantEntityMap,
				VariantScoreMap:  tt.fields.VariantScoreMap,
			}
			got, err := vp.GenerateResponse()
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateResponse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !cmp.Equal(got, tt.want, cmpopts.SortSlices(func(i, j *rankingservicev1.RankingResult) bool {
				if strings.Compare(i.EntityId, j.EntityId) > 0 {
					return true
				} else {
					return false
				}
			})) {
				t.Errorf("GenerateResponse() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIMVariantPipeline_ValidateRequest(t *testing.T) {
	type fields struct {
		Request          *rankingservicev1.RankingRequest
		Config           *configs.Config
		Context          *context.PipelineCtx
		PipelineResponse *executor.PipelineRankingResponse
		VariantEntityMap map[string]*variant.Entity
		VariantScoreMap  map[string]float64
	}
	type args struct {
		request *rankingservicev1.RankingRequest
	}
	currentTime := timestamp.Timestamp{
		Seconds: 1234,
		Nanos:   0,
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		errMsg  string
	}{
		{
			name:   "positive",
			fields: fields{},
			args: args{
				setEntityContext(generateRequestCtx(currentTime), map[string]*any.Any{
					"v1": createAnyProto(&rankingservicev1.RankingVariantInfo{
						VariantInfo: &rankingservicev1.RankingEntityInfo{
							Id:            "v1",
							StoreId:       "s1",
							Orderability:  true,
							OfferPrice:    createMoneyRs(100),
							MrpPrice:      createMoneyRs(150),
							WeightInGrams: 10,
						}}),
					"v2": createAnyProto(&rankingservicev1.RankingVariantInfo{
						VariantInfo: &rankingservicev1.RankingEntityInfo{
							Id:            "v2",
							StoreId:       "s1",
							Orderability:  true,
							OfferPrice:    createMoneyRs(20),
							MrpPrice:      createMoneyRs(150),
							WeightInGrams: 10,
						}}),
				}),
			},
			wantErr: false,
		},
		{
			name:   "negative any incorrect",
			fields: fields{},
			args: args{
				setEntityContext(generateRequestCtx(currentTime), map[string]*any.Any{
					"v1": createAnyProto(&rankingservicev1.RankingEntityInfo{
						Id:            "v1",
						StoreId:       "s1",
						Orderability:  true,
						OfferPrice:    createMoneyRs(100),
						MrpPrice:      createMoneyRs(150),
						WeightInGrams: 10,
					}),
				}),
			},
			wantErr: true,
			errMsg:  "error occurred while unmarshal variant info for v1",
		},
		{
			name:   "negative userId and deviceId not given",
			fields: fields{},
			args: args{
				request: &rankingservicev1.RankingRequest{
					UserClientInfo: &v11.UserClientInfo{UserId: "", DeviceId: ""},
				},
			},
			wantErr: true,
			errMsg:  "userId and deviceId is not present",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vp := &IMVariantPipeline{
				Request:          tt.fields.Request,
				Config:           tt.fields.Config,
				Context:          tt.fields.Context,
				PipelineResponse: tt.fields.PipelineResponse,
				VariantEntityMap: tt.fields.VariantEntityMap,
				VariantScoreMap:  tt.fields.VariantScoreMap,
			}
			if err := vp.ValidateRequest(tt.args.request); (err != nil) != tt.wantErr {
				t.Errorf("ValidateRequest() error = %v, wantErr %v", err, tt.wantErr)
			}
			if err := vp.ValidateRequest(tt.args.request); (err != nil) == true {
				if err.Error() != tt.errMsg {
					t.Errorf("ValidateRequest() error = %v, expected %v", err.Error(), tt.errMsg)
				}
			}

		})
	}
}

func Test_computePercent(t *testing.T) {
	type args struct {
		change *money.Money
		base   *money.Money
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "positive",
			args: args{
				change: createMoneyRs(80),
				base:   createMoneyRs(100),
			},
			want: 20,
		},
		{
			name: "nil",
			args: args{
				change: nil,
				base:   createMoneyRs(100),
			},
			want: 0,
		},
		{
			name: "nil",
			args: args{
				change: nil,
				base:   nil,
			},
			want: 0,
		},
		{
			name: "nil",
			args: args{
				change: createMoneyRs(100),
				base:   nil,
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := computeDiscountPercent(tt.args.change, tt.args.base); got != tt.want {
				t.Errorf("computeDiscountPercent() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_rankEntity(t *testing.T) {
	type args struct {
		entityMap map[string]*variant.Entity
	}
	tests := []struct {
		name string
		args args
		want map[string]float64
	}{
		{
			name: "positive",
			args: args{
				entityMap: map[string]*variant.Entity{
					"v1": {
						EntityId:     "v1",
						VariantId:    "v1",
						StoreId:      "123",
						Orderability: true,
						MrpPrice:     createMoneyRs(100),
						OfferPrice:   createMoneyRs(100),
						WeightValue:  100,
					},
					"v2": {
						EntityId:     "v2",
						VariantId:    "v2",
						StoreId:      "123",
						Orderability: true,
						MrpPrice:     createMoneyRs(100),
						OfferPrice:   createMoneyRs(100),
						WeightValue:  1000,
					},
				},
			},
			want: map[string]float64{
				"v1": 1,
				"v2": 2,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := rankEntity(tt.args.entityMap); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("rankEntity() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_sortVariants(t *testing.T) {
	type args struct {
		entities []*variant.Entity
	}
	tests := []struct {
		name string
		args args
		want []*variant.Entity
	}{
		{
			name: "sorted by offer",
			args: args{
				entities: []*variant.Entity{
					{
						VariantId:    "1",
						StoreId:      "12",
						Orderability: false,
						MrpPrice:     createMoneyRs(12),
						OfferPrice:   createMoneyRs(9),
						WeightValue:  3,
					},
					{
						VariantId:    "2",
						StoreId:      "12",
						Orderability: false,
						MrpPrice:     createMoneyRs(12),
						OfferPrice:   createMoneyRs(10),
						WeightValue:  2,
					},
				},
			},
			want: []*variant.Entity{
				{
					VariantId:    "2",
					StoreId:      "12",
					Orderability: false,
					MrpPrice:     createMoneyRs(12),
					OfferPrice:   createMoneyRs(10),
					WeightValue:  2,
				},
				{
					VariantId:    "1",
					StoreId:      "12",
					Orderability: false,
					MrpPrice:     createMoneyRs(12),
					OfferPrice:   createMoneyRs(9),
					WeightValue:  3,
				},
			},
		},
		{
			name: "sorted by offer then by grammage",
			args: args{
				entities: []*variant.Entity{
					{
						VariantId:    "1",
						StoreId:      "12",
						Orderability: false,
						MrpPrice:     createMoneyRs(12),
						OfferPrice:   createMoneyRs(9),
						WeightValue:  100,
					},
					{
						VariantId:    "2",
						StoreId:      "12",
						Orderability: false,
						MrpPrice:     createMoneyRs(12),
						OfferPrice:   createMoneyRs(9),
						WeightValue:  200,
					},
					{
						VariantId:    "3",
						StoreId:      "12",
						Orderability: false,
						MrpPrice:     createMoneyRs(12),
						OfferPrice:   createMoneyRs(10),
						WeightValue:  50,
					},
				},
			},
			want: []*variant.Entity{
				{
					VariantId:    "3",
					StoreId:      "12",
					Orderability: false,
					MrpPrice:     createMoneyRs(12),
					OfferPrice:   createMoneyRs(10),
					WeightValue:  50,
				},
				{
					VariantId:    "1",
					StoreId:      "12",
					Orderability: false,
					MrpPrice:     createMoneyRs(12),
					OfferPrice:   createMoneyRs(9),
					WeightValue:  100,
				},
				{
					VariantId:    "2",
					StoreId:      "12",
					Orderability: false,
					MrpPrice:     createMoneyRs(12),
					OfferPrice:   createMoneyRs(9),
					WeightValue:  200,
				},
			},
		},
		{
			name: "sorted by offer then by grammage then oos 1",
			args: args{
				entities: []*variant.Entity{
					{
						VariantId:       "1",
						StoreId:         "12",
						Orderability:    true,
						MrpPrice:        createMoneyRs(12),
						OfferPrice:      createMoneyRs(9),
						WeightValue:     100,
						DiscountPercent: 3,
					},
					{
						VariantId:       "2",
						StoreId:         "12",
						Orderability:    true,
						MrpPrice:        createMoneyRs(12),
						OfferPrice:      createMoneyRs(9),
						WeightValue:     200,
						DiscountPercent: 3,
					},
					{
						VariantId:       "3",
						StoreId:         "12",
						Orderability:    true,
						MrpPrice:        createMoneyRs(12),
						OfferPrice:      createMoneyRs(10),
						WeightValue:     50,
						DiscountPercent: 2,
					},
					{
						VariantId:       "4",
						StoreId:         "12",
						Orderability:    false,
						MrpPrice:        createMoneyRs(12),
						OfferPrice:      createMoneyRs(1),
						WeightValue:     500,
						DiscountPercent: 11,
					},
				},
			},
			want: []*variant.Entity{
				{
					VariantId:       "4",
					StoreId:         "12",
					Orderability:    false,
					MrpPrice:        createMoneyRs(12),
					OfferPrice:      createMoneyRs(1),
					WeightValue:     500,
					DiscountPercent: 11,
				},
				{
					VariantId:       "3",
					StoreId:         "12",
					Orderability:    true,
					MrpPrice:        createMoneyRs(12),
					OfferPrice:      createMoneyRs(10),
					WeightValue:     50,
					DiscountPercent: 2,
				},
				{
					VariantId:       "1",
					StoreId:         "12",
					Orderability:    true,
					MrpPrice:        createMoneyRs(12),
					OfferPrice:      createMoneyRs(9),
					WeightValue:     100,
					DiscountPercent: 3,
				},
				{
					VariantId:       "2",
					StoreId:         "12",
					Orderability:    true,
					MrpPrice:        createMoneyRs(12),
					OfferPrice:      createMoneyRs(9),
					WeightValue:     200,
					DiscountPercent: 3,
				},
			},
		},
		{
			name: "sorted by offer then by grammage then oos 2",
			args: args{
				entities: []*variant.Entity{
					{
						VariantId:       "1",
						StoreId:         "12",
						Orderability:    false,
						MrpPrice:        createMoneyRs(12),
						OfferPrice:      createMoneyRs(9),
						WeightValue:     100,
						DiscountPercent: 3,
					},
					{
						VariantId:       "2",
						StoreId:         "12",
						Orderability:    true,
						MrpPrice:        createMoneyRs(12),
						OfferPrice:      createMoneyRs(9),
						WeightValue:     200,
						DiscountPercent: 3,
					},
					{
						VariantId:       "3",
						StoreId:         "12",
						Orderability:    true,
						MrpPrice:        createMoneyRs(12),
						OfferPrice:      createMoneyRs(10),
						WeightValue:     50,
						DiscountPercent: 2,
					},
					{
						VariantId:       "4",
						StoreId:         "12",
						Orderability:    true,
						MrpPrice:        createMoneyRs(12),
						OfferPrice:      createMoneyRs(1),
						WeightValue:     500,
						DiscountPercent: 11,
					},
				},
			},
			want: []*variant.Entity{
				{
					VariantId:       "1",
					StoreId:         "12",
					Orderability:    false,
					MrpPrice:        createMoneyRs(12),
					OfferPrice:      createMoneyRs(9),
					WeightValue:     100,
					DiscountPercent: 3,
				},
				{
					VariantId:       "3",
					StoreId:         "12",
					Orderability:    true,
					MrpPrice:        createMoneyRs(12),
					OfferPrice:      createMoneyRs(10),
					WeightValue:     50,
					DiscountPercent: 2,
				},
				{
					VariantId:       "2",
					StoreId:         "12",
					Orderability:    true,
					MrpPrice:        createMoneyRs(12),
					OfferPrice:      createMoneyRs(9),
					WeightValue:     200,
					DiscountPercent: 3,
				},
				{
					VariantId:       "4",
					StoreId:         "12",
					Orderability:    true,
					MrpPrice:        createMoneyRs(12),
					OfferPrice:      createMoneyRs(1),
					WeightValue:     500,
					DiscountPercent: 11,
				},
			},
		},
		{
			name: "sorted by grammage then oos",
			args: args{
				entities: []*variant.Entity{
					{
						VariantId:       "1",
						StoreId:         "12",
						Orderability:    false,
						MrpPrice:        createMoneyRs(12),
						OfferPrice:      createMoneyRs(9),
						WeightValue:     100,
						DiscountPercent: 0,
					},
					{
						VariantId:       "2",
						StoreId:         "12",
						Orderability:    true,
						MrpPrice:        createMoneyRs(12),
						OfferPrice:      createMoneyRs(9),
						WeightValue:     200,
						DiscountPercent: 0,
					},
					{
						VariantId:       "3",
						StoreId:         "12",
						Orderability:    true,
						MrpPrice:        createMoneyRs(12),
						OfferPrice:      createMoneyRs(9),
						WeightValue:     5000,
						DiscountPercent: 0,
					},
					{
						VariantId:       "4",
						StoreId:         "12",
						Orderability:    true,
						MrpPrice:        createMoneyRs(12),
						OfferPrice:      createMoneyRs(9),
						WeightValue:     500,
						DiscountPercent: 0,
					},
				},
			},
			want: []*variant.Entity{
				{
					VariantId:    "1",
					StoreId:      "12",
					Orderability: false,
					MrpPrice:     createMoneyRs(12),
					OfferPrice:   createMoneyRs(9),
					WeightValue:  100,
				},
				{
					VariantId:    "2",
					StoreId:      "12",
					Orderability: true,
					MrpPrice:     createMoneyRs(12),
					OfferPrice:   createMoneyRs(9),
					WeightValue:  200,
				},
				{
					VariantId:    "4",
					StoreId:      "12",
					Orderability: true,
					MrpPrice:     createMoneyRs(12),
					OfferPrice:   createMoneyRs(9),
					WeightValue:  500,
				},
				{
					VariantId:    "3",
					StoreId:      "12",
					Orderability: true,
					MrpPrice:     createMoneyRs(12),
					OfferPrice:   createMoneyRs(9),
					WeightValue:  5000,
				},
			},
		},
		{
			name: "sorted by oos",
			args: args{
				entities: []*variant.Entity{
					{
						VariantId:       "1",
						StoreId:         "12",
						Orderability:    false,
						MrpPrice:        createMoneyRs(12),
						OfferPrice:      createMoneyRs(9),
						WeightValue:     100,
						DiscountPercent: 3,
					},
					{
						VariantId:       "2",
						StoreId:         "12",
						Orderability:    true,
						MrpPrice:        createMoneyRs(12),
						OfferPrice:      createMoneyRs(9),
						WeightValue:     10,
						DiscountPercent: 3,
					},
				},
			},
			want: []*variant.Entity{
				{
					VariantId:       "1",
					StoreId:         "12",
					Orderability:    false,
					MrpPrice:        createMoneyRs(12),
					OfferPrice:      createMoneyRs(9),
					WeightValue:     100,
					DiscountPercent: 3,
				},
				{
					VariantId:       "2",
					StoreId:         "12",
					Orderability:    true,
					MrpPrice:        createMoneyRs(12),
					OfferPrice:      createMoneyRs(9),
					WeightValue:     10,
					DiscountPercent: 3,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sortVariants(tt.args.entities); !cmp.Equal(got, tt.want, cmpopts.IgnoreUnexported(money.Money{})) {
				t.Errorf("sortVariants() = %+v", cmp.Diff(got, tt.want, cmpopts.IgnoreUnexported(money.Money{})))
			}
		})
	}
}

func TestIMVariantPipeline_EnrichRankingObject(t *testing.T) {
	type fields struct {
		Request          *rankingservicev1.RankingRequest
		Config           *configs.Config
		Context          *context.PipelineCtx
		PipelineResponse *executor.PipelineRankingResponse
		VariantEntityMap map[string]*variant.Entity
		VariantScoreMap  map[string]float64
	}
	tests := []struct {
		name   string
		fields fields
		want   map[string]*variant.Entity
	}{
		{
			name: "positive",
			fields: fields{
				Context: &context.PipelineCtx{
					UserId:   "",
					GeoHash:  "",
					Time:     nil,
					CityId:   "",
					DeviceId: "",
					SwuId:    "",
					Tid:      "",
					Sid:      "",
					EntityObjectMap: map[string]*any.Any{
						"v1": createAnyProto(&rankingservicev1.RankingVariantInfo{
							VariantInfo: &rankingservicev1.RankingEntityInfo{
								Id:            "v1",
								StoreId:       "s1",
								Orderability:  true,
								OfferPrice:    createMoneyRs(50),
								MrpPrice:      createMoneyRs(100),
								WeightInGrams: 10.5,
							},
						}),
						"v2": createAnyProto(&rankingservicev1.RankingVariantInfo{
							VariantInfo: &rankingservicev1.RankingEntityInfo{
								Id:            "v2",
								StoreId:       "s1",
								Orderability:  true,
								OfferPrice:    createMoneyRs(50),
								MrpPrice:      createMoneyRs(100),
								WeightInGrams: 20,
							},
						}),
					},
					ScoreProvider: "",
				},
			},
			want: map[string]*variant.Entity{
				"v1": {
					EntityId:        "v1",
					VariantId:       "v1",
					StoreId:         "s1",
					Orderability:    true,
					MrpPrice:        createMoneyRs(100),
					OfferPrice:      createMoneyRs(50),
					WeightValue:     10.5,
					DiscountPercent: 50,
				},
				"v2": {
					EntityId:        "v2",
					VariantId:       "v2",
					StoreId:         "s1",
					Orderability:    true,
					MrpPrice:        createMoneyRs(100),
					OfferPrice:      createMoneyRs(50),
					WeightValue:     20,
					DiscountPercent: 50,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vp := &IMVariantPipeline{
				Request:          tt.fields.Request,
				Config:           tt.fields.Config,
				Context:          tt.fields.Context,
				PipelineResponse: tt.fields.PipelineResponse,
				VariantEntityMap: tt.fields.VariantEntityMap,
				VariantScoreMap:  tt.fields.VariantScoreMap,
			}
			vp.EnrichRankingObject()
			if !cmp.Equal(vp.VariantEntityMap, tt.want, cmpopts.IgnoreUnexported(money.Money{})) {
				t.Errorf("VariantEntityMap() = %v", cmp.Diff(vp.VariantEntityMap, tt.want, cmpopts.IgnoreUnexported(money.Money{})))
			}
		})
	}
}

func createMoneyRs(i int64) *money.Money {
	return &money.Money{
		CurrencyCode: utils.CURRENCY_INR,
		Units:        i,
		Nanos:        0,
	}
}

func createAnyProto(m proto.Message) *any.Any {
	any, _ := ptypes.MarshalAny(m)
	return any
}

func setEntityContext(request *rankingservicev1.RankingRequest, entityCtx map[string]*any.Any) *rankingservicev1.RankingRequest {
	request.EntityContextMap = entityCtx
	return request
}

func generateRequestCtx(currentTime timestamp.Timestamp) *rankingservicev1.RankingRequest {
	return &rankingservicev1.RankingRequest{
		RequestCtx: &v1.RequestContext{
			ClientId:       "abc",
			MarketPlaceId:  1,
			BusinessLineId: 2,
			CategoryId:     3,
			RequestId:      "aaaa-bbbb",
			RequestTime:    &currentTime,
			LatLng: &latlng.LatLng{
				Latitude:  12.11,
				Longitude: 72.34,
			},
			CityId:   1,
			Referrer: "",
		},
		UserClientInfo: &v11.UserClientInfo{
			DeviceId:         "DeviceId",
			Swuid:            "Swuid",
			Tid:              "tid",
			Ip:               "192.168.0.104",
			Sid:              "sid",
			UserAgent:        "ios",
			UserAgentVersion: "10.9",
			Channel:          "swiggy",
			UserId:           "CS1",
		},
		RankingEntity: 6,
		EntityContextMap: map[string]*any.Any{
			"v1": createAnyProto(&rankingservicev1.RankingEntityInfo{
				Id:            "v1",
				StoreId:       "s1",
				Orderability:  true,
				OfferPrice:    createMoneyRs(100),
				MrpPrice:      createMoneyRs(150),
				WeightInGrams: 10,
			}),
		},
		ScoreProvider: "",
	}
}

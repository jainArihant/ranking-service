package handlers

import (
	"bitbucket.org/swigy/ranking-service/internal/pkg/api"
	"reflect"
	"testing"
)

func TestHealthServer_GetHealth(t *testing.T) {
	type fields struct{
		HealthStatus api.GetHealthResponse_ServingStatus
	}
	tests := []struct {
		name    string
		fields  fields
		want    *api.GetHealthResponse
		wantErr bool
	}{
		{
			name:"success_health_check",
			fields:fields{HealthStatus:api.GetHealthResponse_SERVING_STATUS_UP},
			want:&api.GetHealthResponse{
				Status:               api.GetHealthResponse_SERVING_STATUS_UP,
			},
			wantErr:false,
		},
	}
	for _, tt := range tests{
		handler := &HealthServer{HealthStatus:tt.fields.HealthStatus}
		got, _ := handler.GetHealth(nil,nil)
		if !reflect.DeepEqual(got,tt.want){
			t.Errorf("%v test is failed got %v want %v",tt.name, got, tt.want)
		}
	}
}

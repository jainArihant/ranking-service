package grpcserver

import (
	"bitbucket.org/swigy/ranking-service/internal/pkg/metrics"
	"context"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"runtime/debug"
)

/*
Handles Panic Errors. In case any panic occurs in server, GRPC server will not end abruptly.
*/
func UnaryRecoveryInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		defer func() {
			if r := recover(); r != nil {
				var prometheusInstance metrics.PrometheusServer = metrics.GetPrometheusInstance()
				prometheusInstance.PanicCounter().WithLabelValues(info.FullMethod).Inc()
				log.Error().Msgf("grpc_recovery_error: %v, panic : %v, stack %v", info.FullMethod, r, string(debug.Stack()))
				err = status.Errorf(codes.Internal, "panic in %v", info.FullMethod)
			}
		}()
		return handler(ctx, req)
	}
}

package external

import (
	"bitbucket.org/swigy/ranking-service/internal/pkg/configs"
	"github.com/jarcoal/httpmock"
	"reflect"
	"testing"
)

func TestDSPCaller_CallIMCategoryProduct_And_FetchIMCategoryProduct(t *testing.T) {
	type fields struct {
		Config *configs.Config
	}
	type args struct {
		request *IMCategoryProductReq
	}
	data := getProductPredictResp()
	httpmock.Activate()
	dspHost := "localhost:8000"
	productPredictPath := "/v1/predict-set/instamart_within_category_product_ranking_model"
	defer httpmock.DeactivateAndReset()
	httpmock.RegisterResponder("POST", dspHost+productPredictPath,
		httpmock.NewJsonResponderOrPanic(200, data))
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *IMCategoryProductResp
	}{
		{
			name: "test 1",
			fields: fields{
				Config: &configs.Config{
					AppConfig: configs.AppConfig{},
					LogConfig: configs.LogConfig{},
					ExternalServiceConfig: configs.ExternalServiceConfig{
						DSPHost:                    dspHost,
						IMCategoryProductURL:       productPredictPath,
						IMCategoryProductTimeoutMS: 10000,
					},
				},
			},
			args: args{
				request: &IMCategoryProductReq{RequestList: []*IMCategoryProductInfo{{
					Id: "123",
					Req: &Req{
						CustomerId:  "8807465",
						ListingSlot: 2,
						StoreId:     "1242",
						Sid:         "asfjkbfdakjnkdsf",
						DayOfWeek:   5,
						NodeId:      "asdkjbafd",
						TaxonomyId:  "asfjkbsndajf",
						ProductId:   "193176",
						Geohash:     "tt75h",
						CityId:      6,
					},
				},
				}},
			},
			want: getProductPredictResp(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dspCaller := &DSPCaller{
				Config: tt.fields.Config,
			}
			predictRespChan := dspCaller.CallIMCategoryProduct(tt.args.request)
			imProductResp, err := dspCaller.FetchIMCategoryProduct(predictRespChan)
			if !reflect.DeepEqual(imProductResp, tt.want) {
				t.Errorf("SendEventToDP() , want %v, got %v, err %v", tt.want, predictRespChan, err)
			}
		})
	}
}

func getProductPredictResp() *IMCategoryProductResp {
	return &IMCategoryProductResp{
		Results: []*Result{
			{
				Id:        "193176",
				ScoreInfo: &ScoreInfo{Score: 0.799},
			},
			{
				Id:        "124983",
				ScoreInfo: &ScoreInfo{Score: 0.72},
			},
		},
		Status: 1,
	}
}

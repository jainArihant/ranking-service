package configs

import (
	"github.com/spf13/viper"
)

type LogConfig struct {
	LogLevel              string
	LogMaxSize            int
	LogMaxAge             int
	LogMaxBackupCount     int
	LogCompressionEnabled bool
	LogPath               string
}

func getLogConfig() LogConfig {
	return LogConfig{
		LogLevel:              viper.GetString("LOG_LEVEL"),
		LogMaxSize:            viper.GetInt("LOG_MAX_SIZE"),
		LogMaxAge:             viper.GetInt("LOG_MAX_AGE"),
		LogMaxBackupCount:     viper.GetInt("LOG_MAX_BACKUP_COUNT"),
		LogCompressionEnabled: viper.GetBool("LOG_COMPRESSION_ENABLE"),
		LogPath:               viper.GetString("LOG_FILE_PATH"),
	}
}

func setLogConfigDefaults() {
	viper.SetDefault("LOG_LEVEL", "info")
	viper.SetDefault("LOG_MAX_SIZE", 500)
	viper.SetDefault("LOG_MAX_AGE", 30)
	viper.SetDefault("LOG_MAX_BACKUP_COUNT", 50)
	viper.SetDefault("LOG_COMPRESSION_ENABLE", true)
	viper.SetDefault("LOG_FILE_PATH", "")
}

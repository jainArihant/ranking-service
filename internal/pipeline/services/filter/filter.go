package filter

type RequestFilter interface {
	FilterRequest()
}
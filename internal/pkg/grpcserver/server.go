package grpcserver

import (
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/swigy/ranking-service/internal/handlers/batch"
	_ "github.com/jnewmano/grpc-json-proxy/codec" //added this to enable postman for grpc. Refer this : https://medium.com/@jnewmano/grpc-postman-173b62a64341

	rankingservicev1 "bitbucket.org/swigy/protorepo/storefront/ranking-service/v1"
	"bitbucket.org/swigy/ranking-service/internal/handlers"
	"bitbucket.org/swigy/ranking-service/internal/pipeline"
	"bitbucket.org/swigy/ranking-service/internal/pipeline/variant"
	"bitbucket.org/swigy/ranking-service/internal/pkg/api"
	"bitbucket.org/swigy/ranking-service/internal/pkg/configs"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
)

type Server struct {
	GrpcServer          *grpc.Server
	HealthServer        *handlers.HealthServer
	RankingService      handlers.RankingService
	BatchRankingService batch.BatchRankingService
}

/*
Initialize GRPC Server & its Metrics.
*/
func NewServer(config *configs.Config, promMetrics *grpc_prometheus.ServerMetrics) *Server {
	// Create a gRPC Server with gRPC interceptor.
	unaryServerInterceptors := []grpc.UnaryServerInterceptor{
		promMetrics.UnaryServerInterceptor(),
		UnaryRecoveryInterceptor(),
	}

	gRPCServer := grpc.NewServer(
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(unaryServerInterceptors...)),
		grpc.KeepaliveParams(keepalive.ServerParameters{Time: time.Duration(200) * time.Second}),
		grpc.KeepaliveEnforcementPolicy(keepalive.EnforcementPolicy{PermitWithoutStream: true}),
	)

	rankingService := &handlers.RankingHandler{
		Config: config,
		ServiceMap: map[rankingservicev1.RankingEntity]pipeline.Constructor{
			rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM:         pipeline.NewIMProductPipeline,
			rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT: variant.NewIMVariantPipeline,
		},
	}
	server := &Server{
		GrpcServer:     gRPCServer,
		RankingService: rankingService,
		BatchRankingService: &batch.BatchRankingHandler{
			RankingHandler: rankingService,
			PipelineConfig: map[rankingservicev1.RankingEntity]batch.PipelineConfig{
				rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM: {
					TimeoutMs: config.ProductRankBatchTimeoutMs,
					BatchSize: config.ProductRankBatchSize,
				},
				rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT: {
					TimeoutMs: config.VariantRankBatchTimeoutMs,
					BatchSize: config.VariantRankBatchSize,
				},
			},
		},
	}

	//add handlers.
	rankingservicev1.RegisterRankingAPIServer(gRPCServer, server)
	healthServer := &handlers.HealthServer{
		HealthStatus: api.GetHealthResponse_SERVING_STATUS_DOWN,
	}
	api.RegisterHealthCheckAPIServer(gRPCServer, healthServer)
	server.HealthServer = healthServer
	// Initialize all metrics.
	promMetrics.InitializeMetrics(gRPCServer)

	return server
}

/*
Start GRPC Server.
*/
func (s *Server) Start(config *configs.Config) {
	lis, err := net.Listen("tcp", config.Host+":"+config.Port)
	log.Info().Msgf("Grpc Server Started on %v:%v", config.Host, config.Port)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to listen on port")
	}
	s.HealthServer.HealthStatus = api.GetHealthResponse_SERVING_STATUS_UP

	if err := s.GrpcServer.Serve(lis); err != nil {
		s.HealthServer.HealthStatus = api.GetHealthResponse_SERVING_STATUS_DOWN
		log.Fatal().Err(err).Msg("failed to serve: %s")
	}
}

/*
Handle Graceful Termination of GRPC Server
*/
func ListenForTermination(gRPCServer *grpc.Server) {
	sigChannel := make(chan os.Signal, 1)
	defer signal.Stop(sigChannel)
	signal.Notify(sigChannel, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	cleanUp(sigChannel, gRPCServer)
}

func cleanUp(sigChannel chan os.Signal, gRPCServer *grpc.Server) {
	sgnl := <-sigChannel
	log.Info().Msgf("Signal to shutdown server: %v", sgnl)
	if gRPCServer != nil {
		gRPCServer.GracefulStop()
		log.Info().Msgf("gracefully shutdown the server")
	}
}

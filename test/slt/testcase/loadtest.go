package testcase

import (
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	"os"
)

type TestFile struct {
	TestSuites []TestSuite `json:"test_suites"`
}

type TestSuite struct {
	Path  string   `json:"path"` // Json file with test suite configuration
	Tests []string `json:"tests"`
}

type Test struct {
	TestName  string
	TestSteps []TestStep
}

type ExternalClient struct {
	Name     string            `json:"name"`
	Url      string            `json:"url"`
	Method   string            `json:"method"`
	Request  *json.RawMessage  `json:"request"`
	Response *json.RawMessage  `json:"response"`
	RespUrl  string            `json:"resp_url"`
	Header   map[string]string `json:"headers"`
}

/*
Load Master Test Cases File. Contains location for all test cases.
*/
func LoadTestFile(path string) (*TestFile, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("cannot open test file %v: %v", path, err)
	}
	defer func() {
		if err := file.Close(); err != nil {
			log.Error().Msgf("Unable to close file: %v", path)
		}
	}()

	var t TestFile

	bytes, _ := ioutil.ReadAll(file)
	if err := json.Unmarshal(bytes, &t); err != nil {
		return nil, fmt.Errorf("unexpected JSON in test input: %v", err)
	}

	return &t, nil
}

/*
Load Individual Test Cases.
*/
func LoadTestCase(path string) *Test {
	f, err := os.Open(path)
	if err != nil {
		pwd, _ := os.Getwd()
		log.Error().Msgf("Error opening file err:%v pwd %v", err, pwd)
		return nil
	}
	bytes := make([]byte, 1024*32)
	read, err := f.Read(bytes)
	if err != nil {
		log.Error().Msgf("Error reading file err:%v", err)
		return nil
	}
	test := Test{}
	err = json.Unmarshal(bytes[0:read], &test)
	if err != nil {
		log.Error().Msgf("Error unmarshaling json err:%v", err)
		return nil
	}
	return &test
}

package external

import (
	config "bitbucket.org/swigy/ranking-service/internal/pkg/configs"
	"bitbucket.org/swigy/ranking-service/internal/services/external/client"
	"bytes"
	"encoding/json"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"net/http"
	"time"
)

type EventRequest struct {
	Header *EventHeader `json:"header"`
	Event  interface{}  `json:"event"`
}

type EventResponse struct {
	StatusCode int    `json:"statusCode"`
	Message    string `json:"message"`
	Error      string `json:"error"`
}

type EventHeader struct {
	EventId       int64  `json:"eventId"`
	UUID          string `json:"uuid"`
	AppName       string `json:"appName"`
	TimeStamp     int64  `json:"timeStamp"`
	SchemaVersion string `json:"schemaVersion"`
	Name          string `json:"name"`
}

type EntityScore struct {
	EntityType string  `json:"entity_type"`
	EntityId   string  `json:"entity_id"`
	Score      float64 `json:"score"`
	Rank       int     `json:"rank"`
}

type OOSIMProductDPEvent struct {
	Sid             string         `json:"sid"`
	Tid             string         `json:"tid"`
	DeviceId        string         `json:"deviceId"`
	UserId          string         `json:"userId"`
	EntityScoreList []*EntityScore `json:"entityScoreList"`
	CreatedAt       string         `json:"createdAt"`
}

type DPService interface {
	CallDpEvent(dpEventReq *OOSIMProductDPEvent)
}

type DPServiceImpl struct {
	Config *config.Config
}

func (dp *DPServiceImpl) CallDpEvent(dpEventReq *OOSIMProductDPEvent) {
	resilienceClient := client.GetDPProductPredictHttpClient()
	url := dp.Config.DPHost
	eventReq := EventRequest{
		Header: &EventHeader{
			UUID:          uuid.New().String(),
			AppName:       dp.Config.DPApplicationName,
			TimeStamp:     time.Now().UnixNano() / int64(time.Millisecond),
			SchemaVersion: dp.Config.DPOOSProductEventSchemaVersion,
			Name:          dp.Config.DPOOSProductEventName,
		},
		Event: dpEventReq,
	}
	log.Info().Interface("event_req", dpEventReq).Msgf("event request for dp")
	requestBody, err := json.Marshal(eventReq)
	if err != nil {
		log.Error().Msgf("error in getting requestbody %v", err)
		return
	}
	headers := http.Header{
		"content-type": []string{"application/json"},
	}
	httpRequest, err := http.NewRequest("POST", url, bytes.NewBuffer(requestBody))
	if err != nil {
		log.Error().Msgf("error in getting httpRequest %v", err)
		return
	}
	httpRequest.Header = headers
	go resilienceClient.Do(httpRequest)
}

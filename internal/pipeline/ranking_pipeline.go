package pipeline

import (
	rankingservicev1 "bitbucket.org/swigy/protorepo/storefront/ranking-service/v1"
	"bitbucket.org/swigy/ranking-service/internal/pipeline/services/context"
	"bitbucket.org/swigy/ranking-service/internal/pipeline/services/enrichment"
	"bitbucket.org/swigy/ranking-service/internal/pipeline/services/executor"
	"bitbucket.org/swigy/ranking-service/internal/pipeline/services/external"
	"bitbucket.org/swigy/ranking-service/internal/pipeline/services/filter"
	"bitbucket.org/swigy/ranking-service/internal/pipeline/services/transformer"
	"bitbucket.org/swigy/ranking-service/internal/pipeline/services/validation"
	"bitbucket.org/swigy/ranking-service/internal/pkg/configs"
)

type RankingPipeline interface {
	validation.RequestValidator
	context.PipelineCtxBuilder
	external.ExternalCaller
	enrichment.RankingEnrichment
	filter.RequestFilter
	executor.RankingExecutor
	transformer.ResponseGenerator
}

type Constructor func(*configs.Config, *rankingservicev1.RankingRequest) RankingPipeline

package configs

import (
	"github.com/spf13/viper"
	"time"
)

type ExternalServiceConfig struct {
	DSPHost                                   string
	IMCategoryProductURL                      string
	IMCategoryProductTimeoutMS                time.Duration
	IMCategoryProductMaxConcurrentRequest     int64
	IMCategoryProductErrorThresholdPercentage int64
	IMCategoryProductRequestVolumeThreshold   int64

	DPHost                                    string
	DPOOSProductEventTimeoutMS                time.Duration
	DPOOSProductEventMaxConcurrentRequest     int64
	DPOOSProductEventErrorThresholdPercentage int64
	DPOOSProductEventRequestVolumeThreshold   int64
	DPApplicationName                         string
	DPOOSProductEventSchemaVersion            string
	DPOOSProductEventName                     string
}

func getExternalServiceConfig() ExternalServiceConfig {
	return ExternalServiceConfig{
		DSPHost:                                   viper.GetString("DSP_HOST"),
		IMCategoryProductURL:                      viper.GetString("IM_CATEGORY_PRODUCT_URL"),
		IMCategoryProductTimeoutMS:                viper.GetDuration("IM_CATEGORY_PRODUCT_TIMEOUT_MS"),
		IMCategoryProductMaxConcurrentRequest:     viper.GetInt64("IM_CATEGORY_PRODUCT_REQUEST_MAX_CONCURRENT_REQUEST"),
		IMCategoryProductErrorThresholdPercentage: viper.GetInt64("IM_CATEGORY_PRODUCT_ERROR_THRESHOLD_PERCENTAGE"),
		IMCategoryProductRequestVolumeThreshold:   viper.GetInt64("IM_CATEGORY_PRODUCT_REQUEST_VOLUME_THRESHOLD"),

		DPHost:                                    viper.GetString("DP_HOST"),
		DPOOSProductEventTimeoutMS:                viper.GetDuration("DP_OOS_PRODUCT_EVENT_TIMEOUT_MS"),
		DPOOSProductEventMaxConcurrentRequest:     viper.GetInt64("DP_OOS_PRODUCT_EVENT_MAX_CONCURRENT_REQUEST"),
		DPOOSProductEventErrorThresholdPercentage: viper.GetInt64("DP_OOS_PRODUCT_EVENT_ERROR_THRESHOLD_PERCENTAGE"),
		DPOOSProductEventRequestVolumeThreshold:   viper.GetInt64("DP_OOS_PRODUCT_EVENT_REQUEST_VOLUME_THRESHOLD"),
		DPApplicationName:                         viper.GetString("DP_APPLICATION_NAME"),
		DPOOSProductEventSchemaVersion:            viper.GetString("DP_OOS_PRODUCT_EVENT_SCHEMA_VERSION"),
		DPOOSProductEventName:                     viper.GetString("DP_OOS_PRODUCT_EVENT_NAME"),
	}
}

func setExternalServiceConfigDefault() {
	viper.SetDefault("DSP_HOST", "https://vidura-runtime.staging.singapore.swig.gy")
	viper.SetDefault("IM_CATEGORY_PRODUCT_URL", "/v1/predict-set/instamart_within_category_product_ranking_model")
	viper.SetDefault("IM_CATEGORY_PRODUCT_TIMEOUT_MS", 500)
	viper.SetDefault("IM_CATEGORY_PRODUCT_REQUEST_MAX_CONCURRENT_REQUEST", 1000)
	viper.SetDefault("IM_CATEGORY_PRODUCT_ERROR_THRESHOLD_PERCENTAGE", 50)
	viper.SetDefault("IM_CATEGORY_PRODUCT_REQUEST_VOLUME_THRESHOLD", 100)

	viper.SetDefault("DP_HOST", "http://localhost:9001/message")
	viper.SetDefault("DP_OOS_PRODUCT_EVENT_TIMEOUT_MS", 500)
	viper.SetDefault("DP_OOS_PRODUCT_EVENT_MAX_CONCURRENT_REQUEST", 1000)
	viper.SetDefault("DP_OOS_PRODUCT_EVENT_ERROR_THRESHOLD_PERCENTAGE", 50)
	viper.SetDefault("DP_OOS_PRODUCT_EVENT_REQUEST_VOLUME_THRESHOLD", 100)
	viper.SetDefault("DP_APPLICATION_NAME", "ranking-service")
	viper.SetDefault("DP_OOS_PRODUCT_EVENT_SCHEMA_VERSION", "1.0.0")
	viper.SetDefault("DP_OOS_PRODUCT_EVENT_NAME", "OOSEntityScore")
}

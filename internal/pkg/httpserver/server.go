package httpserver

import (
	"bitbucket.org/swigy/ranking-service/internal/pkg/configs"
	"bitbucket.org/swigy/ranking-service/internal/pkg/metrics"
	"github.com/etherlabsio/healthcheck"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog/log"
	"net/http"
)

type HttpServer struct {
	*http.ServeMux
	*configs.Config
}

/*
Configure Http Server for prometheus.
*/
func NewServer(config *configs.Config, prometheusServer metrics.PrometheusServer) *HttpServer {
	// Create a HTTP server for prometheus.
	serveMux := http.NewServeMux()
	serveMux.Handle("/metrics", promhttp.HandlerFor(prometheusServer.Registry(), promhttp.HandlerOpts{}))
	serveMux.Handle("/health-check", healthcheck.Handler())

	return &HttpServer{
		ServeMux: serveMux,
		Config:   config,
	}
}

/*
Start Http server.
*/
func (s *HttpServer) Start() {

	if err := http.ListenAndServe(s.Config.Host+":"+s.Config.HTTPPort, s.ServeMux); err != nil {
		log.Fatal().Err(err).Msg("Unable to start http server")
	}
}

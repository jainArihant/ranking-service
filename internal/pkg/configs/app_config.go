package configs

import (
	"github.com/spf13/viper"
)

type AppConfig struct {
	AppName  string
	Host     string
	Port     string
	HTTPPort string
}

func getAppConfig() AppConfig {
	return AppConfig{
		AppName:  "ranking-service",
		Host:     viper.GetString("HOST"),
		Port:     viper.GetString("GRPC_PORT"),
		HTTPPort: viper.GetString("HTTP_PORT"),
	}
}

func setAppConfigDefaults() {
	viper.SetDefault("HOST","0.0.0.0")
	viper.SetDefault("GRPC_PORT","8282")
	viper.SetDefault("HTTP_PORT","8383")
}

package logger

import (
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gopkg.in/natefinch/lumberjack.v2"
	"io"
	"os"
	"strings"
)

const (
	unSupportedJSONMarshallType = "unsupported json marshalling type"
	emptyStruct                 = "{}"
)

func InitZeroLog(serviceName string, logfile io.Writer) {
	log.Logger = zerolog.New(logfile).With().Timestamp().Caller().Str("application", serviceName).Logger()
}

// Please input maxSize in MBs and maxAge in days
func InitZeroLogWithFile(serviceName string, logPath string, maxSize int, maxAge int, maxBackups int, compress bool) error {
	if logPath == "" {
		return nil
	}
	_, err := os.OpenFile(logPath, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		InitZeroLog(serviceName, os.Stdout)
		return fmt.Errorf("cannot open file: %v: %v", logPath, err.Error())
	}
	logFile := &lumberjack.Logger{
		Filename:   logPath,
		MaxSize:    maxSize,
		MaxAge:     maxAge,
		MaxBackups: maxBackups,
		Compress:   compress,
	}
	InitZeroLog(serviceName, logFile)
	return nil
}

func GetSampler(samplingRate uint32) zerolog.Sampler {
	return &zerolog.BasicSampler{N: samplingRate}
}

func MarshalToJSON(event *zerolog.Event, name string, value interface{}) {
	if value == nil {
		event.Str(name, "")
		return
	}
	field, err := json.Marshal(value)
	if err != nil {
		event.Str(name, unSupportedJSONMarshallType)
		return
	}
	if string(field) == emptyStruct {
		event.Str(name, fmt.Sprintf("%#v", value))
		return
	}
	event.RawJSON(name, field)
}

func SetLogLevel(logLevel string) {
	result := log.Logger
	logLevel = strings.ToLower(logLevel)

	switch logLevel {
	case "", "info":
		result = result.Level(zerolog.InfoLevel)
	case "warn":
		result = result.Level(zerolog.WarnLevel)
	case "debug":
		result = result.Level(zerolog.DebugLevel)
	default:
		result = result.Level(zerolog.InfoLevel)
	}
	log.Logger = result
}


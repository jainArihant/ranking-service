package main

import (
	conf "bitbucket.org/swigy/ranking-service/internal/pkg/configs"
	"bitbucket.org/swigy/ranking-service/internal/pkg/grpcserver"
	"bitbucket.org/swigy/ranking-service/internal/pkg/httpserver"
	"bitbucket.org/swigy/ranking-service/internal/pkg/logger"
	"bitbucket.org/swigy/ranking-service/internal/pkg/metrics"
	_ "github.com/golang/protobuf/ptypes/wrappers"
	_ "github.com/jnewmano/grpc-json-proxy/codec"
	"github.com/rs/zerolog/log"
)

func main() {

	//Initialise Configs, Metrics & Log
	config := conf.InitConfig()
	logger.InitLogger(config)
	pServer := metrics.InitPrometheus()

	//Initialise GRPC & HTTP Servers
	hTTPServer := httpserver.NewServer(config, pServer)
	gRPCServer := grpcserver.NewServer(config, pServer.CreateServerMetrics())
	log.Info().Msg("Starting Ranking-Service...")

	//Start GRPC & HTTP Servers
	go hTTPServer.Start()
	go gRPCServer.Start(config)

	//Handle Graceful Termination
	grpcserver.ListenForTermination(gRPCServer.GrpcServer)
}

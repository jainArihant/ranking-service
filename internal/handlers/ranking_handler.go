package handlers

import (
	"fmt"

	"bitbucket.org/swigy/ranking-service/internal/pkg/metrics"
	"github.com/prometheus/client_golang/prometheus"

	rankingservicev1 "bitbucket.org/swigy/protorepo/storefront/ranking-service/v1"
	"bitbucket.org/swigy/ranking-service/internal/pipeline"
	"bitbucket.org/swigy/ranking-service/internal/pkg/configs"
	"github.com/rs/zerolog/log"
)

type RankingService interface {
	Rank(request *rankingservicev1.RankingRequest) (response *rankingservicev1.RankingResponse,
		err error)
}

type RankingHandler struct {
	Config     *configs.Config
	ServiceMap map[rankingservicev1.RankingEntity]pipeline.Constructor
}

func (rh *RankingHandler) Rank(request *rankingservicev1.RankingRequest) (response *rankingservicev1.RankingResponse, err error) {
	defer RecoverFromPanic("Rank")
	log.Info().Interface("ranking_request", request).Msgf("Ranking Request")
	timer := prometheus.NewTimer(metrics.GetPrometheusInstance().MethodRequestLatency().WithLabelValues("rank-time", request.GetRankingEntity().String()))
	defer timer.ObserveDuration()

	pipelineConstructor := rh.ServiceMap[request.RankingEntity]
	if pipelineConstructor == nil {
		log.Error().Msgf("no service found for entity %v", request.RankingEntity)
		return nil, fmt.Errorf(fmt.Sprintf("no service found for entity %v", request.RankingEntity))
	}
	service := pipelineConstructor(rh.Config, request)
	err = service.ValidateRequest(request)
	if err != nil {
		log.Error().Msgf("Error occurred while validating request %v", err)
		return nil, err
	}
	err = service.BuildContext()
	if err != nil {
		log.Error().Msgf("Error occurred while building context %v", err)
		return nil, err
	}
	service.CallExternal()
	service.EnrichRankingObject()
	service.FilterRequest()
	service.ExecuteRanking()
	resp, err := service.GenerateResponse()
	if log.Debug().Enabled() {
		log.Debug().Interface("ranking_response", resp).Msgf("ranking_response")
	}
	return resp, err
}

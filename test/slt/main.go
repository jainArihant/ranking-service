package main

import (
	"bitbucket.org/swigy/ranking-service/test/slt/client"
	"bitbucket.org/swigy/ranking-service/test/slt/testcase"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"
)

const testFilesPath = "test/slt/files/"

/*
This main is executed to run functional tests.
*/
func main() {
	// Create Client
	c := &client.RankingServiceClient{}
	if err := c.Init("0.0.0.0:8282"); err != nil {
		log.Fatal().Err(err)
	}

	// Load All Test files.
	testFiles, _ := testcase.LoadTestFile(testFilesPath + "slt_list.json")

	// Execute Test cases one by one.
	for i, suite := range testFiles.TestSuites {
		log.Info().Msgf("Executing suite (%v/%v) : %v", i+1, len(testFiles.TestSuites), suite.Path)
		for t, test := range suite.Tests {
			testCase := testcase.LoadTestCase(testFilesPath + suite.Path + "/" + test)
			log.Info().Msgf("Executing test (%v/%v): %v", t+1, len(suite.Tests), test)
			executeTest(testCase, c)
			log.Info().Msg(" ====Test Case : Passed==== ")
		}
	}
}

/*
Execute Individual test case using reflection.
*/
func executeTest(test *testcase.Test, client *client.RankingServiceClient) {
	for i := range test.TestSteps {
		assert := test.TestSteps[i]
		ExternalCalls(assert.ExternalClients)
		log.Info().Msgf("Calling api: %v", assert.ApiCall)
		val := make([]reflect.Value, 1)
		val[0] = reflect.ValueOf(assert)
		reflect.ValueOf(client).MethodByName(assert.ApiCall).Call(val)
	}
}

func ExternalCalls(externalClients []testcase.ExternalClient) []string {
	var ids []string
	id := ""
	for _, extClient := range externalClients {
		//extClient.Header["Content-Type"] = "application/json"
		if extClient.Method == "POST" {
			reqReader, _ := json.Marshal(extClient.Request)
			reqReaderbuf := strings.NewReader(string(reqReader))
			log.Info().Interface("req", reqReaderbuf).Msgf("req")
			respBody, err := CallHttp("POST", extClient.Url, reqReaderbuf, extClient.Header)
			if err != nil {
				continue
			}
			id = strings.TrimSpace(string(respBody))
		} else if extClient.Method == "GET" {
			var resp []byte
			var err error
			reqReader, _ := json.Marshal(extClient.Request)
			if string(reqReader) != "{}" && string(reqReader) != "null" {
				reqReaderbuf := strings.NewReader(string(reqReader))
				resp, err = CallHttp("GET", extClient.Url, reqReaderbuf, map[string]string{})
			} else {
				resp, err = CallHttp("GET", extClient.Url, nil, map[string]string{})
			}
			if err != nil {
				continue
			}
			id = strings.TrimSpace(string(resp))
		}
		respreader, _ := json.Marshal(extClient.Response)
		respreaderbuf := strings.NewReader(string(respreader))
		http.Post(extClient.RespUrl+id, "", respreaderbuf)
		ids = append(ids, id)
	}
	return ids
}

func CallHttp(method string, url string, reqBody io.Reader, header map[string]string) ([]byte, error) {
	client := http.Client{}
	request, err := http.NewRequest(method, url, reqBody)
	request.Close = true
	for k, v := range header {
		request.Header.Set(k, v)
	}
	if err != nil {
		log.Error().Msgf("%v", err)
		return nil, err
	}

	resp, err := client.Do(request)
	if err != nil {
		log.Error().Msgf("%v", err)
		return nil, err
	}
	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}

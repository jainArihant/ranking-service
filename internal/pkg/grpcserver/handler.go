package grpcserver

import (
	"context"

	rankingservicev1 "bitbucket.org/swigy/protorepo/storefront/ranking-service/v1"
)

func (s *Server) Ranking(ctx context.Context, request *rankingservicev1.RankingRequest) (response *rankingservicev1.
	RankingResponse, err error) {
	return s.RankingService.Rank(request)
}

func (s *Server) BatchRank(ctx context.Context, request *rankingservicev1.BatchRankRequest) (*rankingservicev1.BatchRankResponse, error) {
	return s.BatchRankingService.BatchRank(request)
}

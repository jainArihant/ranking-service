#!/bin/bash

# Exit if any error
set -e

# Install Docker-compose
if [ $# -eq 1 ]
then
    curl -L https://github.com/docker/compose/releases/download/1.25.3/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
else
    docker-compose -f deployments/local/docker-compose.yml down -v
    docker rmi $(docker images --filter=reference='com.swiggy/dash-sf/ranking-service*' -q) | true
fi

docker network create shared | true

echo "Starting local setup"
docker-compose -f deployments/local/docker-compose.yml down -v
make slt-clean slt-build
COMPOSE_HTTP_TIMEOUT=200 docker-compose -f deployments/local/docker-compose.yml up --build -d --force-recreate --remove-orphans

# Sleep to allow dependency dockers to load
echo "Sleeping for local setup to come up"

sleep 10

docker ps -a

docker logs -f ranking-service &

make slt-run

docker stop ranking-service

sleep 20

cp /tmp/server/slt-coverage.out ./coverage | true

echo "Stopping local setup"
docker-compose -f deployments/local/docker-compose.yml down

echo "Functional coverage report - start"
go tool cover -func=./coverage/slt-coverage.out
echo "Functional coverage report - done"
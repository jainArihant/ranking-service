package variant

import (
	"fmt"
	"sort"
	"strconv"

	"bitbucket.org/swigy/ranking-service/api/variant"

	"bitbucket.org/swigy/ranking-service/internal/utils"

	sortAlg "bitbucket.org/swigy/ranking-service/internal/services/sort"

	"github.com/mmcloughlin/geohash"

	"bitbucket.org/swigy/ranking-service/internal/pipeline"

	"google.golang.org/genproto/googleapis/type/money"

	rankingservicev1 "bitbucket.org/swigy/protorepo/storefront/ranking-service/v1"
	"bitbucket.org/swigy/ranking-service/internal/pipeline/services/context"
	"bitbucket.org/swigy/ranking-service/internal/pipeline/services/executor"
	"bitbucket.org/swigy/ranking-service/internal/pkg/configs"
	"github.com/golang/protobuf/ptypes"
	"github.com/rs/zerolog/log"
)

type IMVariantPipeline struct {
	Request          *rankingservicev1.RankingRequest
	Config           *configs.Config
	Context          *context.PipelineCtx
	PipelineResponse *executor.PipelineRankingResponse
	VariantEntityMap map[string]*variant.Entity
	VariantScoreMap  map[string]float64
}

func NewIMVariantPipeline(config *configs.Config, request *rankingservicev1.RankingRequest) pipeline.RankingPipeline {
	return &IMVariantPipeline{
		Config:  config,
		Request: request,
	}
}

func (vp *IMVariantPipeline) ValidateRequest(request *rankingservicev1.RankingRequest) error {
	if request.GetUserClientInfo().GetUserId() == "" && request.GetUserClientInfo().GetDeviceId() == "" {
		return fmt.Errorf("userId and deviceId is not present")
	}

	for entityId, object := range request.EntityContextMap {
		var rankingVariantInfo rankingservicev1.RankingVariantInfo
		err := ptypes.UnmarshalAny(object, &rankingVariantInfo)
		if err != nil {
			log.Error().Msgf("error occurred while unmarshal variant info for %v", entityId)
			return fmt.Errorf("error occurred while unmarshal variant info for %v", entityId)
		}
	}
	return nil
}

func (vp *IMVariantPipeline) BuildContext() error {
	request := vp.Request

	vp.Context = &context.PipelineCtx{}

	if request.GetRequestCtx() != nil &&
		request.GetRequestCtx().GetLatLng() != nil {
		vp.Context.GeoHash = geohash.EncodeWithPrecision(request.GetRequestCtx().GetLatLng().GetLatitude(),
			request.GetRequestCtx().GetLatLng().GetLongitude(), 9)
	}

	if request.GetUserClientInfo() != nil {
		vp.Context.DeviceId = request.GetUserClientInfo().GetDeviceId()
		vp.Context.Tid = request.GetUserClientInfo().GetTid()
		vp.Context.Sid = request.GetUserClientInfo().GetSid()
		vp.Context.UserId = request.GetUserClientInfo().GetUserId()
		vp.Context.SwuId = request.GetUserClientInfo().GetSwuid()

	}

	if request.GetRequestCtx() != nil {
		vp.Context.Time = request.GetRequestCtx().GetRequestTime()
		vp.Context.CityId = strconv.FormatInt(int64(request.GetRequestCtx().GetCityId()), 10)
	}

	vp.Context.ScoreProvider = request.GetScoreProvider()
	vp.Context.EntityObjectMap = request.GetEntityContextMap()

	return nil
}

func (vp *IMVariantPipeline) CallExternal() {
	return
}

func (vp *IMVariantPipeline) EnrichRankingObject() {

	entitymap := make(map[string]*variant.Entity, 1)
	entityObjectMap := vp.Context.EntityObjectMap
	for entityId, object := range entityObjectMap {
		var entitySortingInfo rankingservicev1.RankingVariantInfo
		err := ptypes.UnmarshalAny(object, &entitySortingInfo)
		if err != nil {
			log.Error().Msgf("error occurred while unmarshal entity sorting info for %v", entityId)
			continue
		}
		variantEntity := variant.Entity{
			EntityId:     entityId,
			VariantId:    entitySortingInfo.GetVariantInfo().GetId(),
			StoreId:      entitySortingInfo.GetVariantInfo().GetStoreId(),
			Orderability: entitySortingInfo.GetVariantInfo().GetOrderability(),
			MrpPrice:     entitySortingInfo.GetVariantInfo().GetMrpPrice(),
			OfferPrice:   entitySortingInfo.GetVariantInfo().GetOfferPrice(),
			WeightValue:  entitySortingInfo.GetVariantInfo().GetWeightInGrams(),
			DiscountPercent: computeDiscountPercent(entitySortingInfo.GetVariantInfo().GetOfferPrice(),
				entitySortingInfo.GetVariantInfo().GetMrpPrice()),
		}

		entitymap[entityId] = &variantEntity
	}
	vp.VariantEntityMap = entitymap
	return
}

func (vp *IMVariantPipeline) FilterRequest() {
	return
}

func (vp *IMVariantPipeline) ExecuteRanking() {
	entityIdRankMap := rankEntity(vp.VariantEntityMap)
	vp.VariantScoreMap = entityIdRankMap
}

func (vp *IMVariantPipeline) GenerateResponse() (*rankingservicev1.RankingResponse, error) {
	var rankingResults []*rankingservicev1.RankingResult

	for variantId, score := range vp.VariantScoreMap {
		rankingResults = append(rankingResults, &rankingservicev1.RankingResult{
			EntityId: variantId,
			Score:    score,
		})
	}

	return &rankingservicev1.RankingResponse{
		RankingObjects: rankingResults,
	}, nil
}

func rankEntity(entityMap map[string]*variant.Entity) map[string]float64 {
	entityRankMap := map[string]float64{}
	var variantEntities []*variant.Entity
	for _, entity := range entityMap {
		variantEntities = append(variantEntities, entity)
	}
	variantEntities = sortVariants(variantEntities)
	score := float64(0)
	for _, entity := range variantEntities {
		score++
		entityRankMap[entity.EntityId] = score
	}
	return entityRankMap
}

// Sort variants from least to highest
func sortVariants(entities []*variant.Entity) []*variant.Entity {
	sort.Sort(sortAlg.VariantSort(entities))
	return entities
}

func computeDiscountPercent(change *money.Money, base *money.Money) float64 {
	if change == nil || base == nil {
		return 0
	}

	baseRs := utils.FloatValueFromGoogleMoneyFormat(base)
	changeRs := utils.FloatValueFromGoogleMoneyFormat(change)

	if baseRs == 0 || changeRs == 0 {
		return 0
	}
	return utils.ComputePercent(changeRs, baseRs)
}

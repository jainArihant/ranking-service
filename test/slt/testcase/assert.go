package testcase

import (
	"encoding/json"
	"reflect"

	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"github.com/rs/zerolog/log"
	"go.uber.org/zap"
	status2 "google.golang.org/genproto/googleapis/rpc/status"
	"google.golang.org/grpc/status"
)

type TestStep struct {
	ApiCall         string
	ExternalClients []ExternalClient
	Argument        json.RawMessage
	Expected        json.RawMessage
	Error           json.RawMessage
}

/*
Generic function to assert expected error & expected Response.
	First Asserts Error (If error is expected, response need not be validated)
	Then Asserts Response.
*/
func (step *TestStep) AssertOutput(apiErr error, response proto.Message, expected proto.Message) {

	expectedErr := getErrorFromJson(step.Error)

	if expectedErr != nil {
		assertError(apiErr, expectedErr)
	} else {
		assertResponse(response, expected, step.Expected)
	}
}

func getErrorFromJson(expectedError json.RawMessage) error {
	exp := &status2.Status{}

	if err := json.Unmarshal(expectedError, exp); err != nil {
		log.Fatal().Msg("Error in unmarshal ")
	}

	return status.FromProto(exp).Err()
}

/*
Assert Expected Error with Received Error.
*/
func assertError(apiErr error, expectedError error) {
	if expectedError == nil {
		return
	} else if apiErr == nil {
		log.Fatal().Msgf("Expected Error : %v, Actual Error: %v", expectedError.Error(), "nil")
	} else if !reflect.DeepEqual(apiErr.Error(), expectedError.Error()) {
		log.Fatal().Msgf("Expected Error : %v, Actual Error: %v", expectedError.Error(), apiErr.Error())
	}
}

/*
Assert Expected Response with Received Response.
*/
func assertResponse(response proto.Message, expectedResp proto.Message, rawExpectedMsg json.RawMessage) {
	rawMsgToExpectedResponse(expectedResp, rawExpectedMsg)

	expectedResponse, actualResponse := UnMarshalResponse(expectedResp), UnMarshalResponse(response)
	if !cmp.Equal(expectedResponse, actualResponse, cmpopts.SortMaps(func(s1, s2 string) bool { return s1 > s2 })) {
		log.Info().Interface("expected", expectedResponse).Msgf("expected")
		log.Info().Interface("actual", actualResponse).Msgf("actual")
		log.Fatal().Msgf("Expected Response : %v, Actual Response : %v", zap.Any("ExpectedResponse", expectedResponse),
			zap.Any("ActualResponse", actualResponse))
	}
}

func rawMsgToExpectedResponse(expectedResp proto.Message, rawExpectedMsg json.RawMessage) {
	bytes, err := rawExpectedMsg.MarshalJSON()
	if err != nil {
		log.Fatal().Err(err).Msgf("MarshalJSON error")
	}

	err = jsonpb.UnmarshalString(string(bytes), expectedResp)
	if err != nil {
		log.Fatal().Err(err).Msgf("UnmarshalString error")
	}
}

func UnMarshalResponse(input proto.Message) map[string]interface{} {
	response := map[string]interface{}{}

	responseBytes, err := (&jsonpb.Marshaler{}).MarshalToString(input)

	if err != nil {
		log.Fatal().Err(err).Msgf("Marshal error")
	}

	err = json.Unmarshal([]byte(responseBytes), &response)

	if err != nil {
		log.Fatal().Err(err).Msgf("UnMarshal error")
	}
	return response
}

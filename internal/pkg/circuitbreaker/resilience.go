package circuitbreaker

import (
	"bitbucket.org/swigy/ranking-service/internal/pkg/metrics"
	"context"
	"errors"
	"fmt"
	"github.com/cep21/circuit/v3"
	"github.com/cep21/circuit/v3/closers/hystrix"
	"github.com/gojektech/heimdall"
	"github.com/gojektech/heimdall/httpclient"
	"github.com/jiacai2050/prometrics"
	"github.com/rs/zerolog/log"
	"net/http"
	"regexp"
	"time"
)

var (
	defaultHttpTimeout            = 1000 * time.Millisecond
	defaultRetryMaxBackoffTimeout = 500 * time.Millisecond
	defaultCircuitName            = "UNNAMED-CIRCUIT"
	defaultExponentFactor         = 2.0
)

/**
Resilience config is the abstraction to expose resiliency levers to the app irresepective of the underlying circuit breaker library
heimdall, hystrix-go and circuit are the three comparable implementations which can be chosen based on what turns out to be the
most performant, and actively developed project in the open source.
*/
type ResilienceConfig struct {
	HttpTimeout            time.Duration
	RetryCount             int
	RetryMaxBackoffTimeout time.Duration
	MaxConcurrentRequests  int64
	// ErrorThresholdPercentage is https://github.com/Netflix/Hystrix/wiki/Configuration#circuitbreakererrorthresholdpercentage
	ErrorThresholdPercentage int64
	// RequestVolumeThreshold is https://github.com/Netflix/Hystrix/wiki/Configuration#circuitbreakerrequestvolumethreshold
	RequestVolumeThreshold int64
	Name                   string
}

func NewResilienceConfig(httpTimeout time.Duration, retryCount int, retryMaxBackoffTimeout time.Duration,
	maxConcurrentRequests int64, errorThresholdPercentage int64, requestVolumeThreshold int64, name string) ResilienceConfig {
	if httpTimeout == 0 {
		httpTimeout = defaultHttpTimeout
	}
	if retryMaxBackoffTimeout == 0 {
		retryMaxBackoffTimeout = defaultRetryMaxBackoffTimeout
	}
	if name == "" {
		name = defaultCircuitName
	}
	//Circuit already has sensible defaults
	return ResilienceConfig{
		HttpTimeout:              httpTimeout,
		RetryCount:               retryCount,
		RetryMaxBackoffTimeout:   retryMaxBackoffTimeout,
		MaxConcurrentRequests:    maxConcurrentRequests,
		ErrorThresholdPercentage: errorThresholdPercentage,
		RequestVolumeThreshold:   requestVolumeThreshold,
		Name:                     name,
	}
}

/**
Resiliency configs should ideally be defined on a per api call level. However to make the least amount of changes to introduce
it in this codebase, it is attempted at http client level.
Current implementation uses circuit the one that comes as guidance from AOH
https://docs.google.com/document/d/1PpH79XV9lmW6CwVygO0c4sEdkOIhD5jNg7yKoH24XkQ/edit#heading=h.vulysbkq4h25
*/
type ResilientHttpClient struct {
	httpClient httpclient.Client
	circuit    circuit.Circuit
}

func NewResilientHttpClient(resilienceConfig ResilienceConfig) ResilientHttpClient {
	httpClient := httpclient.NewClient(
		httpclient.WithHTTPTimeout(resilienceConfig.HttpTimeout),
		httpclient.WithRetryCount(resilienceConfig.RetryCount),
		httpclient.WithRetrier(
			heimdall.NewRetrier(
				heimdall.NewExponentialBackoff(resilienceConfig.HttpTimeout, resilienceConfig.RetryMaxBackoffTimeout,
					defaultExponentFactor, resilienceConfig.HttpTimeout))))
	prom := prometrics.GetFactory(metrics.GetPrometheusInstance().Registry())
	circuitConfig := prom.CommandProperties(resilienceConfig.Name)
	circuitConfig.General.ClosedToOpenFactory = hystrix.OpenerFactory(hystrix.ConfigureOpener{
		ErrorThresholdPercentage: resilienceConfig.ErrorThresholdPercentage,
		RequestVolumeThreshold: resilienceConfig.RequestVolumeThreshold,
	})
	circuitConfig.General.OpenToClosedFactory = hystrix.CloserFactory(hystrix.ConfigureCloser{})
	circuitConfig.Execution.MaxConcurrentRequests = resilienceConfig.MaxConcurrentRequests
	circuitConfig.Execution.Timeout = time.Duration(resilienceConfig.RetryCount+1) * resilienceConfig.HttpTimeout //Using circuit timeout the same as retries * http timeout
	c := circuit.NewCircuitFromConfig(resilienceConfig.Name, circuitConfig)
	log.Info().Interface("resilienceConfig", resilienceConfig).Interface("circuitConfig", circuitConfig).Msgf(
		"Created ResilientHttpClient and circuitConfig")
	return ResilientHttpClient{*httpClient, *c}
}

func (c *ResilientHttpClient) Do(request *http.Request) (*http.Response, error) {
	var httpResponse *http.Response
	errResult := c.circuit.Execute(context.Background(), func(ctx context.Context) error {
		var err error
		if httpResponse, err = c.httpClient.Do(request); err != nil {
			return err
		}
		/*
			Needs more changes in consumer to handle circuit BadRequest than 4xx status code directly
			This code and corresponding test can be uncommented once that is completed.

			if ok, _ := regexp.MatchString(`4..`, httpResponse.Status); ok {
				return circuit.SimpleBadRequest{errors.New("4xx response code received -" + httpResponse.Status)}
			}*/
		if ok, _ := regexp.MatchString(`5..`, httpResponse.Status); ok {
			return errors.New("5xx response code received -" + httpResponse.Status)
		}
		return err
	}, func(ctx context.Context, err error) error {
		//Not using any fallback
		log.Error().Msgf("Fallback triggered for circuit %v and url %v with error %v ", c.circuit.Name(), request.RequestURI, err)
		return errors.New(fmt.Sprintf("Fallback triggered for circuit %v and url %v with error %v ", c.circuit.Name(), request.RequestURI, err))
	})
	return httpResponse, errResult
}

func (c *ResilientHttpClient) Get(url string, headers http.Header) (*http.Response, error) {
	/** Delegating actual work to Do method */
	var response *http.Response
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return response, errors.New("GET - request creation failed")
	}
	request.Header = headers
	return c.Do(request)
}

type AsyncResponse struct {
	Resp *http.Response
	Err  error
}

func (c *ResilientHttpClient) AsyncDo(request *http.Request, asyncResp chan *AsyncResponse) {
	resp, err := c.Do(request)
	asyncResp <- &AsyncResponse{
		Resp: resp,
		Err:  err,
	}
}

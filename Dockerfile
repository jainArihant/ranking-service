FROM alpine:latest
RUN apk --no-cache add ca-certificates
RUN export GOPRIVATE="bitbucket.org/swigy/*"

RUN apk add --no-cache tzdata
ENV TZ="Asia/Kolkata"

RUN mkdir -p /tmp/server
RUN chmod 666 /tmp/server

RUN mkdir -p /var/log/ranking-service
RUN chmod 0666 -R /var/log/ranking-service

#use ssh instead of https
WORKDIR /
ADD bin /
RUN ls -ltr /
EXPOSE 8282
ENTRYPOINT ["./ranking-service"]
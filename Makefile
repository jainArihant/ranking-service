PROJECT_ROOT ?= $(shell pwd)
BUILD_VARIANT ?= release
BUILD_DIR ?= $(PROJECT_ROOT)/bin
SERVER_DIR ?= $(PROJECT_ROOT)/cmd
GOBUILD ?= GO111MODULE=on go build
BINARY_NAME ?= ranking-service
COVERAGE_DIR ?= $(PROJECT_ROOT)/coverage
GOTEST_BASE_CMD ?= GO111MODULE=on go test
GOTEST_FLAGS ?= -covermode=count
GOTEST ?= $(GOTEST_BASE_CMD) $(GOTEST_FLAGS)
SLT_DIR ?= $(PROJECT_ROOT)/test/slt

.PHONY: all init build test slt-build slt-run slt-clean

all: init build

init:
	git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"
	git submodule update --init --remote --recursive

build:
	@ CGO_ENABLED=0 GOARCH=amd64 GOOS=linux $(GOBUILD) -o $(BUILD_DIR)/$(BINARY_NAME) $(SERVER_DIR)

test:
	@echo "Running unit tests..."
	@mkdir -p $(COVERAGE_DIR)
	@$(GOTEST) -short -coverprofile=$(COVERAGE_DIR)/unit-coverage.out ./... -count=1

slt-build:
	@echo "Compiling binary for test coverage."
	@ CGO_ENABLED=0 GOARCH=amd64 GOOS=linux $(GOTEST) -c -o $(BUILD_DIR)/$(BINARY_NAME)-slt $(SERVER_DIR) -coverpkg=./... -count=1

slt-run:
	@echo "Running slt tests..."
	$(GOBUILD) -o $(BUILD_DIR)/$(BINARY_NAME) $(SLT_DIR)
	@$(PROJECT_ROOT)/bin/ranking-service

slt:
	@echo "Running functional tests..."
	@mkdir -p $(COVERAGE_DIR)
	@sh slt.sh

slt-clean:
	@rm -f $(PROJECT_ROOT)/bin/ranking-service
	@rm -f $(PROJECT_ROOT)/bin/ranking-service-slt
	@rm -f $(COVERAGE_DIR)/slt-coverage.out
	@GO111MODULE=on go clean ./...

build-mock-server:
	@echo "Building mock server ..."
	@GO111MODULE=on go build -o ./mockserver.o ./test/mockserver/
	@chmod 777 ./mockserver.o

start-mock-server:
	@echo "Starting mock server ..."
	./mockserver.o
package utils

import (
	"github.com/golang/protobuf/ptypes/timestamp"
	"testing"
	"time"
)

func TestGetValidTime(t *testing.T) {
	got := GetValidTime(&timestamp.Timestamp{Seconds: 20, Nanos: 50})
	if !got.Equal(time.Unix(20, 50)) {
		t.Errorf("Failure")
	}
}


func TestListingSlot_CheckDefineTimeSlot( t *testing.T){
	baseTime, _ := time.Parse("2006-01-02 15:04:05", "1995-05-22 13:10:05")
	val := GetListingSlot(baseTime)
	if val != 2{
		t.Errorf("Got: %v Want: %v", val, 2)
	}
}

func TestListingSlot_CheckDefaultTimeSlot( t *testing.T){
	baseTime, _ := time.Parse("2006-01-02 15:04:05", "1995-05-22 05:10:05")
	val := GetListingSlot(baseTime)
	if val != 7{
		t.Errorf("Got: %v Want: %v", val, 2)
	}
}


func TestContains(t *testing.T) {
	team := []string{"arnav", "sumit", "prakash"}
	ooo := "anirudh"
	got := Contains(team, ooo)
	if got {
		t.Errorf("Got: %v Want: %v", got, false)
	}
	in := "arnav"
	got = Contains(team, in)
	if !got {
		t.Errorf("Got: %v Want: %v", got, true)
	}
}

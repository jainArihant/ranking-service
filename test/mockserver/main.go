package main

import (
	mockServer "bitbucket.org/swigy/go-mock-server/pkg"
)

func main() {
	server := mockServer.GetServer(false, 12345, false, 0)
	server.Start()
}

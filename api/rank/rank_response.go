package rank

import rankingservicev1 "bitbucket.org/swigy/protorepo/storefront/ranking-service/v1"

type BatchRankResponse struct {
	EntityGroupId string
	Response      *rankingservicev1.RankingResponse
	Err           error
}

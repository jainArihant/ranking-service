package metrics

import (
	"sync"

	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/prometheus/client_golang/prometheus"
)

const NameSpace = "swiggy"
const SubSystem = "dash"

type PrometheusServer interface {
	CreateServerMetrics() *grpc_prometheus.ServerMetrics
	CreateCounterMetricsVec(name string, helpString string, labelNames []string) *prometheus.CounterVec
	CreateHistogramMetricsVec(name string, helpString string, labelNames []string, buckets []float64) *prometheus.HistogramVec
	CreateGaugeMetricsVec(name string, helpString string, labelNames []string) *prometheus.GaugeVec
	Registry() *prometheus.Registry
	MethodRequestCounter() *prometheus.CounterVec
	MethodRequestLatency() *prometheus.HistogramVec
	MethodExceptionCounter() *prometheus.CounterVec
	BatchEntityCounter() *prometheus.HistogramVec
	PanicCounter() *prometheus.CounterVec
	ExternalServiceHistogramMetrics() *prometheus.HistogramVec
	GenericCounter() *prometheus.CounterVec
}

type prometheusServer struct {
	registry                        *prometheus.Registry
	methodRequestCounter            *prometheus.CounterVec
	methodRequestLatency            *prometheus.HistogramVec
	methodExceptionCounter          *prometheus.CounterVec
	batchEntityCounter              *prometheus.HistogramVec
	panicCounter                    *prometheus.CounterVec
	externalServiceHistogramMetrics *prometheus.HistogramVec
	genericCounter                  *prometheus.CounterVec
}

var instance *prometheusServer
var once sync.Once

func GetPrometheusInstance() *prometheusServer {
	once.Do(func() {
		instance = &prometheusServer{}
		instance.registry = prometheus.NewRegistry()
		instance.methodRequestCounter = instance.CreateCounterMetricsVec("method_requests_total",
			"Requests received, partitioned by different go method", []string{"method_name", "method_operation"})
		instance.genericCounter = instance.CreateCounterMetricsVec("generic_counter",
			"Requests received, partitioned by different go method", []string{"counter_name"})
		instance.methodRequestLatency = instance.CreateHistogramMetricsVec("method_requests_latency",
			"Requests Latency histogram, partitioned by different go method", []string{"method_name", "method_operation"},
			[]float64{0.001, 0.002, 0.005, 0.010, 0.015, 0.020, 0.025, 0.030, 0.035, 0.040, 0.05, 0.075, 0.1, 0.25, 0.5, 1, 2.5, 5})
		instance.methodExceptionCounter = instance.CreateCounterMetricsVec("method_exception_total",
			"Method requests error, partitioned by different go method and error code", []string{"method_name", "method_operation", "error_code"})
		instance.batchEntityCounter = instance.CreateHistogramMetricsVec("batch_entity_count",
			"Batch entity histogram, partitioned by different grpc method, entity type", []string{"entity_name"},
			[]float64{0, 1, 2, 5, 10, 15, 25, 35, 50, 75, 100, 150, 250, 400, 600, 1000, 1500, 2000, 2500, 3000})
		instance.panicCounter = instance.CreateCounterMetricsVec("panic_counter_total",
			"Total number of panics called", []string{"method_name"})
	})
	return instance
}

func InitPrometheus() *prometheusServer {
	instance = GetPrometheusInstance()
	return instance
}

func (ps *prometheusServer) CreateServerMetrics() *grpc_prometheus.ServerMetrics {
	grpcMetrics := grpc_prometheus.NewServerMetrics()
	buckets := grpc_prometheus.WithHistogramBuckets([]float64{0.001, 0.002, 0.005, 0.010, 0.015, 0.020, 0.025, 0.030,
		0.035, 0.040, 0.05, 0.075, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.75, 1.0, 2.5, 5})
	grpcMetrics.EnableHandlingTimeHistogram(buckets)
	ps.registry.MustRegister(prometheus.NewProcessCollector(prometheus.ProcessCollectorOpts{}), prometheus.NewGoCollector(),
		grpcMetrics)
	return grpcMetrics
}

func (ps *prometheusServer) CreateCounterMetricsVec(name string, helpString string, labelNames []string) *prometheus.CounterVec {
	customizedCounterMetric := prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: NameSpace,
		Subsystem: SubSystem,
		Name:      name,
		Help:      helpString,
	}, labelNames)
	ps.registry.MustRegister(customizedCounterMetric)
	return customizedCounterMetric
}

func (ps *prometheusServer) CreateHistogramMetricsVec(name string, helpString string, labelNames []string, buckets []float64) *prometheus.HistogramVec {
	customizedHistogramMetrics := prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: NameSpace,
		Subsystem: SubSystem,
		Name:      name,
		Help:      helpString,
		Buckets:   buckets,
	}, labelNames)
	ps.registry.MustRegister(customizedHistogramMetrics)
	return customizedHistogramMetrics
}

func (ps *prometheusServer) CreateGaugeMetricsVec(name string, helpString string, labelNames []string) *prometheus.GaugeVec {
	customizedGaugeMetrics := prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: NameSpace,
		Subsystem: SubSystem,
		Name:      name,
		Help:      helpString,
	}, labelNames)
	ps.registry.MustRegister(customizedGaugeMetrics)
	return customizedGaugeMetrics
}

func (ps *prometheusServer) Registry() *prometheus.Registry {
	return ps.registry
}

func (ps *prometheusServer) MethodRequestCounter() *prometheus.CounterVec {
	return ps.methodRequestCounter
}

func (ps *prometheusServer) MethodRequestLatency() *prometheus.HistogramVec {
	return ps.methodRequestLatency
}

func (ps *prometheusServer) MethodExceptionCounter() *prometheus.CounterVec {
	return ps.methodExceptionCounter
}

func (ps *prometheusServer) BatchEntityCounter() *prometheus.HistogramVec {
	return ps.batchEntityCounter
}

func (ps *prometheusServer) PanicCounter() *prometheus.CounterVec {
	return ps.panicCounter
}

func (ps *prometheusServer) ExternalServiceHistogramMetrics() *prometheus.HistogramVec {
	return ps.externalServiceHistogramMetrics
}

func (ps *prometheusServer) GenericCounter() *prometheus.CounterVec {
	return ps.genericCounter
}

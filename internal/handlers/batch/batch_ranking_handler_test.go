package batch

import (
	"errors"
	"reflect"
	"testing"
	"time"

	"bitbucket.org/swigy/ranking-service/internal/handlers"

	"github.com/golang/protobuf/ptypes/timestamp"

	"bitbucket.org/swigy/ranking-service/internal/utils"
	"google.golang.org/genproto/googleapis/type/money"

	"github.com/golang/protobuf/proto"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"

	v1 "bitbucket.org/swigy/protorepo/platform/bundle-commons/marketplace/v1"
	sharedv1 "bitbucket.org/swigy/protorepo/storefront/storefront-shared/v1"
	"google.golang.org/genproto/googleapis/type/latlng"

	rankingservicev1 "bitbucket.org/swigy/protorepo/storefront/ranking-service/v1"
)

func TestBatchRankingHandler_BatchRank(t *testing.T) {
	type fields struct {
		RankingHandler handlers.RankingService
		PipelineConfig map[rankingservicev1.RankingEntity]PipelineConfig
	}
	type args struct {
		request *rankingservicev1.BatchRankRequest
	}
	tests := []struct {
		name    string
		args    args
		want    *rankingservicev1.BatchRankResponse
		wantErr bool
		mocker  func() fields
	}{
		{
			name: "success",
			args: args{
				request: &rankingservicev1.BatchRankRequest{
					RequestCtx:     createRequestCtx(),
					RankingEntity:  rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT,
					UserClientInfo: createUserClientInfo(),
					RankRequests: []*rankingservicev1.RankRequestEntity{
						{
							EntityGroupId: "group1",
							EntityContextMap: map[string]*any.Any{
								"v1": createAnyProto(createRankingVariantInfo("v1", "123", true, 80, 100, 1000)),
								"v2": createAnyProto(createRankingVariantInfo("v2", "123", true, 50, 100, 1000)),
							},
							ScoreProvider: "",
						},
						{
							EntityGroupId: "group2",
							EntityContextMap: map[string]*any.Any{
								"v3": createAnyProto(createRankingVariantInfo("v3", "123", true, 50, 100, 100)),
								"v4": createAnyProto(createRankingVariantInfo("v4", "123", true, 50, 100, 1000)),
							},
							ScoreProvider: "",
						},
					},
				},
			},
			want: &rankingservicev1.BatchRankResponse{
				Results: map[string]*rankingservicev1.RankingResponse{
					"group1": {
						RankingObjects: []*rankingservicev1.RankingResult{
							&rankingservicev1.RankingResult{
								EntityId: "v1",
								Meta:     nil,
								Score:    1,
							},
							&rankingservicev1.RankingResult{
								EntityId: "v2",
								Meta:     nil,
								Score:    2,
							},
						},
					},
					"group2": {
						RankingObjects: []*rankingservicev1.RankingResult{
							&rankingservicev1.RankingResult{
								EntityId: "v3",
								Meta:     nil,
								Score:    1,
							},
							&rankingservicev1.RankingResult{
								EntityId: "v4",
								Meta:     nil,
								Score:    2,
							},
						},
					},
				},
			},
			mocker: func() fields {
				mockService := &handlers.MockRankingService{}
				var err error
				err = nil
				request1 := createRankingRequest(createRequestCtx(),
					createUserClientInfo(),
					rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT,
					map[string]*any.Any{
						"v1": createAnyProto(createRankingVariantInfo("v1", "123", true, 80, 100, 1000)),
						"v2": createAnyProto(createRankingVariantInfo("v2", "123", true, 50, 100, 1000)),
					})
				resp1 := &rankingservicev1.RankingResponse{
					RankingObjects: []*rankingservicev1.RankingResult{
						&rankingservicev1.RankingResult{
							EntityId: "v1",
							Meta:     nil,
							Score:    1,
						},
						&rankingservicev1.RankingResult{
							EntityId: "v2",
							Meta:     nil,
							Score:    2,
						},
					},
				}
				mockService.On("Rank", request1).Return(resp1, err)

				request2 := createRankingRequest(createRequestCtx(),
					createUserClientInfo(),
					rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT,
					map[string]*any.Any{
						"v3": createAnyProto(createRankingVariantInfo("v3", "123", true, 50, 100, 100)),
						"v4": createAnyProto(createRankingVariantInfo("v4", "123", true, 50, 100, 1000)),
					})
				resp2 := &rankingservicev1.RankingResponse{
					RankingObjects: []*rankingservicev1.RankingResult{
						&rankingservicev1.RankingResult{
							EntityId: "v3",
							Meta:     nil,
							Score:    1,
						},
						&rankingservicev1.RankingResult{
							EntityId: "v4",
							Meta:     nil,
							Score:    2,
						},
					},
				}
				mockService.On("Rank", request2).Return(resp2, err)
				return fields{
					RankingHandler: mockService,
				}
			},
			wantErr: false,
		},
		{
			name: "1 Rank failed",
			args: args{
				request: &rankingservicev1.BatchRankRequest{
					RequestCtx:     createRequestCtx(),
					RankingEntity:  rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT,
					UserClientInfo: createUserClientInfo(),
					RankRequests: []*rankingservicev1.RankRequestEntity{
						{
							EntityGroupId: "group1",
							EntityContextMap: map[string]*any.Any{
								"v1": createAnyProto(createRankingVariantInfo("v1", "123", true, 80, 100, 1000)),
								"v2": createAnyProto(createRankingVariantInfo("v2", "123", true, 50, 100, 1000)),
							},
							ScoreProvider: "",
						},
						{
							EntityGroupId: "group2",
							EntityContextMap: map[string]*any.Any{
								"v3": createAnyProto(createRankingVariantInfo("v3", "123", true, 50, 100, 100)),
								"v4": createAnyProto(createRankingVariantInfo("v4", "123", true, 50, 100, 1000)),
							},
							ScoreProvider: "",
						},
					},
				},
			},
			want: &rankingservicev1.BatchRankResponse{
				Results: map[string]*rankingservicev1.RankingResponse{
					"group1": {
						RankingObjects: []*rankingservicev1.RankingResult{
							&rankingservicev1.RankingResult{
								EntityId: "v1",
								Meta:     nil,
								Score:    1,
							},
							&rankingservicev1.RankingResult{
								EntityId: "v2",
								Meta:     nil,
								Score:    2,
							},
						},
					},
				},
			},
			wantErr: false,
			mocker: func() fields {
				mockService := &handlers.MockRankingService{}
				var err error
				err = nil
				request1 := createRankingRequest(createRequestCtx(),
					createUserClientInfo(),
					rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT,
					map[string]*any.Any{
						"v1": createAnyProto(createRankingVariantInfo("v1", "123", true, 80, 100, 1000)),
						"v2": createAnyProto(createRankingVariantInfo("v2", "123", true, 50, 100, 1000)),
					})
				resp1 := &rankingservicev1.RankingResponse{
					RankingObjects: []*rankingservicev1.RankingResult{
						&rankingservicev1.RankingResult{
							EntityId: "v1",
							Meta:     nil,
							Score:    1,
						},
						&rankingservicev1.RankingResult{
							EntityId: "v2",
							Meta:     nil,
							Score:    2,
						},
					},
				}
				mockService.On("Rank", request1).Return(resp1, err)

				request2 := createRankingRequest(createRequestCtx(),
					createUserClientInfo(),
					rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT,
					map[string]*any.Any{
						"v3": createAnyProto(createRankingVariantInfo("v3", "123", true, 50, 100, 100)),
						"v4": createAnyProto(createRankingVariantInfo("v4", "123", true, 50, 100, 1000)),
					})

				mockService.On("Rank", request2).Return(nil, errors.New("error occurred"))
				return fields{
					RankingHandler: mockService,
				}
			},
		},
		{
			name: "All RankRequest failed",
			args: args{
				request: &rankingservicev1.BatchRankRequest{
					RequestCtx:     createRequestCtx(),
					RankingEntity:  rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT,
					UserClientInfo: createUserClientInfo(),
					RankRequests: []*rankingservicev1.RankRequestEntity{
						{
							EntityGroupId: "group1",
							EntityContextMap: map[string]*any.Any{
								"v1": createAnyProto(createRankingVariantInfo("v1", "123", true, 80, 100, 1000)),
								"v2": createAnyProto(createRankingVariantInfo("v2", "123", true, 50, 100, 1000)),
							},
							ScoreProvider: "",
						},
						{
							EntityGroupId: "group2",
							EntityContextMap: map[string]*any.Any{
								"v3": createAnyProto(createRankingVariantInfo("v3", "123", true, 50, 100, 100)),
								"v4": createAnyProto(createRankingVariantInfo("v4", "123", true, 50, 100, 1000)),
							},
							ScoreProvider: "",
						},
					},
				},
			},
			want:    nil,
			wantErr: true,
			mocker: func() fields {
				mockService := &handlers.MockRankingService{}
				request1 := createRankingRequest(createRequestCtx(),
					createUserClientInfo(),
					rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT,
					map[string]*any.Any{
						"v1": createAnyProto(createRankingVariantInfo("v1", "123", true, 80, 100, 1000)),
						"v2": createAnyProto(createRankingVariantInfo("v2", "123", true, 50, 100, 1000)),
					})

				mockService.On("Rank", request1).Return(nil, errors.New("error occurred in solving this"))

				request2 := createRankingRequest(createRequestCtx(),
					createUserClientInfo(),
					rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT,
					map[string]*any.Any{
						"v3": createAnyProto(createRankingVariantInfo("v3", "123", true, 50, 100, 100)),
						"v4": createAnyProto(createRankingVariantInfo("v4", "123", true, 50, 100, 1000)),
					})

				mockService.On("Rank", request2).Return(nil, errors.New("error occurred in solving this"))
				return fields{
					RankingHandler: mockService,
				}
			},
		},
		{
			name: "Validation failed",
			args: args{
				request: &rankingservicev1.BatchRankRequest{
					RequestCtx:     createRequestCtx(),
					RankingEntity:  rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT,
					UserClientInfo: createUserClientInfo(),
					RankRequests: []*rankingservicev1.RankRequestEntity{
						{
							EntityGroupId: "group1",
							EntityContextMap: map[string]*any.Any{
								"v1": createAnyProto(createRankingVariantInfo("v1", "123", true, 80, 100, 1000)),
								"v2": createAnyProto(createRankingVariantInfo("v2", "123", true, 50, 100, 1000)),
							},
							ScoreProvider: "",
						},
						{
							EntityGroupId: "group2",
							EntityContextMap: map[string]*any.Any{
								"v3": createAnyProto(createRankingVariantInfo("v3", "123", true, 50, 100, 100)),
								"v4": createAnyProto(createRankingVariantInfo("v4", "123", true, 50, 100, 1000)),
							},
							ScoreProvider: "",
						},
					},
				},
			},
			want:    nil,
			wantErr: true,
			mocker: func() fields {
				mockService := &handlers.MockRankingService{}
				request1 := createRankingRequest(createRequestCtx(),
					createUserClientInfo(),
					rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT,
					map[string]*any.Any{
						"v1": createAnyProto(createRankingVariantInfo("v1", "123", true, 80, 100, 1000)),
						"v2": createAnyProto(createRankingVariantInfo("v2", "123", true, 50, 100, 1000)),
					})

				mockService.On("Rank", request1).Return(nil, errors.New("error occurred in solving this"))

				request2 := createRankingRequest(createRequestCtx(),
					createUserClientInfo(),
					rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT,
					map[string]*any.Any{
						"v3": createAnyProto(createRankingVariantInfo("v3", "123", true, 50, 100, 100)),
						"v4": createAnyProto(createRankingVariantInfo("v4", "123", true, 50, 100, 1000)),
					})

				mockService.On("Rank", request2).Return(nil, errors.New("error occurred in solving this"))
				return fields{
					RankingHandler: mockService,
					PipelineConfig: map[rankingservicev1.RankingEntity]PipelineConfig{
						rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT: {
							TimeoutMs: 0,
							BatchSize: 1,
						},
					},
				}
			},
		},
		{
			name: "1 failed coz of timeout",
			args: args{
				request: &rankingservicev1.BatchRankRequest{
					RequestCtx:     createRequestCtx(),
					RankingEntity:  rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT,
					UserClientInfo: createUserClientInfo(),
					RankRequests: []*rankingservicev1.RankRequestEntity{
						{
							EntityGroupId: "group1",
							EntityContextMap: map[string]*any.Any{
								"v1": createAnyProto(createRankingVariantInfo("v1", "123", true, 80, 100, 1000)),
								"v2": createAnyProto(createRankingVariantInfo("v2", "123", true, 50, 100, 1000)),
							},
							ScoreProvider: "",
						},
						{
							EntityGroupId: "group2",
							EntityContextMap: map[string]*any.Any{
								"v3": createAnyProto(createRankingVariantInfo("v3", "123", true, 50, 100, 100)),
								"v4": createAnyProto(createRankingVariantInfo("v4", "123", true, 50, 100, 1000)),
							},
							ScoreProvider: "",
						},
					},
				},
			},
			want: &rankingservicev1.BatchRankResponse{
				Results: map[string]*rankingservicev1.RankingResponse{
					"group1": {
						RankingObjects: []*rankingservicev1.RankingResult{
							&rankingservicev1.RankingResult{
								EntityId: "v1",
								Meta:     nil,
								Score:    1,
							},
							&rankingservicev1.RankingResult{
								EntityId: "v2",
								Meta:     nil,
								Score:    2,
							},
						},
					},
				},
			},
			mocker: func() fields {
				mockService := &handlers.MockRankingService{}
				var err error
				err = nil
				request1 := createRankingRequest(createRequestCtx(),
					createUserClientInfo(),
					rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT,
					map[string]*any.Any{
						"v1": createAnyProto(createRankingVariantInfo("v1", "123", true, 80, 100, 1000)),
						"v2": createAnyProto(createRankingVariantInfo("v2", "123", true, 50, 100, 1000)),
					})
				resp1 := &rankingservicev1.RankingResponse{
					RankingObjects: []*rankingservicev1.RankingResult{
						&rankingservicev1.RankingResult{
							EntityId: "v1",
							Meta:     nil,
							Score:    1,
						},
						&rankingservicev1.RankingResult{
							EntityId: "v2",
							Meta:     nil,
							Score:    2,
						},
					},
				}
				mockService.On("Rank", request1).Return(resp1, err)

				request2 := createRankingRequest(createRequestCtx(),
					createUserClientInfo(),
					rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT,
					map[string]*any.Any{
						"v3": createAnyProto(createRankingVariantInfo("v3", "123", true, 50, 100, 100)),
						"v4": createAnyProto(createRankingVariantInfo("v4", "123", true, 50, 100, 1000)),
					})
				resp2 := &rankingservicev1.RankingResponse{
					RankingObjects: []*rankingservicev1.RankingResult{
						&rankingservicev1.RankingResult{
							EntityId: "v3",
							Meta:     nil,
							Score:    1,
						},
						&rankingservicev1.RankingResult{
							EntityId: "v4",
							Meta:     nil,
							Score:    2,
						},
					},
				}
				mockService.On("Rank", request2).Return(resp2, err).WaitUntil(time.After(100 * time.Millisecond))
				return fields{
					RankingHandler: mockService,
					PipelineConfig: map[rankingservicev1.RankingEntity]PipelineConfig{
						rankingservicev1.RankingEntity_RANKING_ENTITY_STORE_ITEM_VARIANT: {
							TimeoutMs: 10,
						},
					},
				}
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fields := tt.mocker()
			rh := &BatchRankingHandler{
				RankingHandler: fields.RankingHandler,
				PipelineConfig: fields.PipelineConfig,
			}
			got, err := rh.BatchRank(tt.args.request)
			if (err != nil) != tt.wantErr {
				t.Errorf("BatchRank() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BatchRank() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func createUserClientInfo() *sharedv1.UserClientInfo {
	return &sharedv1.UserClientInfo{
		DeviceId:         "12345678",
		Swuid:            "12345678",
		Tid:              "ffff-gggg-hhhhh",
		Ip:               "192.168.0.104",
		Sid:              "rrrr-tttt-yyyy",
		UserAgent:        "Swiggy-Android",
		UserAgentVersion: "1",
		Channel:          "",
		UserId:           "12345",
	}
}

func createRequestCtx() *v1.RequestContext {
	return &v1.RequestContext{
		ClientId:       "",
		MarketPlaceId:  1,
		BusinessLineId: 7,
		CategoryId:     3,
		RequestId:      "aaaa-bbbb-cccc-dddd",
		RequestTime: &timestamp.Timestamp{
			Seconds: 1222,
			Nanos:   0,
		},
		LatLng: &latlng.LatLng{
			Latitude:  12.99,
			Longitude: 77.22,
		},
		CityId: 1,
	}
}

func createRankingVariantInfo(id string, storeId string, orderable bool, mrp int64, offerPrice int64, wig float64) *rankingservicev1.RankingVariantInfo {
	return &rankingservicev1.RankingVariantInfo{
		VariantInfo: &rankingservicev1.RankingEntityInfo{
			Id:            id,
			StoreId:       storeId,
			Orderability:  orderable,
			OfferPrice:    createMoneyRs(offerPrice),
			MrpPrice:      createMoneyRs(mrp),
			WeightInGrams: wig,
		},
	}
}

func createRankingRequest(ctx *v1.RequestContext, userCInfo *sharedv1.UserClientInfo, entity rankingservicev1.RankingEntity, ecm map[string]*any.Any) *rankingservicev1.RankingRequest {
	return &rankingservicev1.RankingRequest{
		RequestCtx:       ctx,
		UserClientInfo:   userCInfo,
		RankingEntity:    entity,
		EntityContextMap: ecm,
		ScoreProvider:    "",
	}
}

func createAnyProto(m proto.Message) *any.Any {
	any, _ := ptypes.MarshalAny(m)
	return any
}

func createMoneyRs(i int64) *money.Money {
	return &money.Money{
		CurrencyCode: utils.CURRENCY_INR,
		Units:        i,
		Nanos:        0,
	}
}

package logger

import (
	config "bitbucket.org/swigy/ranking-service/internal/pkg/configs"
	"fmt"
	"os"
)

func InitLogger(config *config.Config) {
	err := InitZeroLogWithFile(config.AppName, config.LogPath, config.LogMaxSize, config.LogMaxAge, config.LogMaxBackupCount, config.LogCompressionEnabled)
	if err != nil {
		fmt.Printf("Cannot initialize logging: %v", err.Error())
		os.Exit(-1)
	}
	SetLogLevel(config.LogLevel)
}

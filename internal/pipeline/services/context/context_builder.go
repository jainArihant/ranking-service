package context

import (
	"github.com/golang/protobuf/ptypes/any"
	"github.com/golang/protobuf/ptypes/timestamp"
)

type PipelineCtx struct {
	UserId          string
	GeoHash         string
	Time            *timestamp.Timestamp
	CityId          string
	DeviceId        string
	SwuId           string
	Tid             string
	Sid             string
	EntityObjectMap map[string]*any.Any
	ScoreProvider   string
}

type PipelineCtxBuilder interface {
	BuildContext() error
}

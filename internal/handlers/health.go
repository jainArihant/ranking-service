package handlers

import (
	"bitbucket.org/swigy/ranking-service/internal/pkg/api"
	"context"
)

type HealthServer struct {
	HealthStatus api.GetHealthResponse_ServingStatus
}

func (server *HealthServer) GetHealth(context.Context, *api.GetHealthRequest) (*api.GetHealthResponse, error) {
	return &api.GetHealthResponse{Status:  server.HealthStatus}, nil
}

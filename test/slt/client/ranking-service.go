package client

import (
	"context"
	"strings"

	rankingservicev1 "bitbucket.org/swigy/protorepo/storefront/ranking-service/v1"
	"bitbucket.org/swigy/ranking-service/test/slt/testcase"
	"github.com/golang/protobuf/jsonpb"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
)

type RankingServiceClient struct {
	conn          *grpc.ClientConn
	RankingClient rankingservicev1.RankingAPIClient
}

func (client *RankingServiceClient) Init(address string) error {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		return err
	}
	client.RankingClient = rankingservicev1.NewRankingAPIClient(conn)
	client.conn = conn
	return nil
}

func (c *RankingServiceClient) Rank(step testcase.TestStep) {
	in := rankingservicev1.RankingRequest{}
	out := rankingservicev1.RankingResponse{}

	byteData, err := step.Argument.MarshalJSON()
	if err != nil {
		log.Fatal().Err(err)
	}

	if err := jsonpb.Unmarshal(strings.NewReader(string(byteData)), &in); err != nil {
		log.Fatal().Err(err).Msg("Test Case UnMarshal Error")
	}
	response, apiErr := c.RankingClient.Ranking(context.Background(), &in)

	//Assert Response & Errors
	step.AssertOutput(apiErr, response, &out)
}

func (c *RankingServiceClient) BatchRank(step testcase.TestStep) {
	in := rankingservicev1.BatchRankRequest{}
	out := rankingservicev1.BatchRankResponse{}

	byteData, err := step.Argument.MarshalJSON()
	if err != nil {
		log.Fatal().Err(err)
	}

	if err := jsonpb.Unmarshal(strings.NewReader(string(byteData)), &in); err != nil {
		log.Fatal().Err(err).Msg("Test Case UnMarshal Error")
	}
	response, apiErr := c.RankingClient.BatchRank(context.Background(), &in)

	//Assert Response & Errors
	step.AssertOutput(apiErr, response, &out)
}

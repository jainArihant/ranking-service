package transformer

import (
	rankingservicev1 "bitbucket.org/swigy/protorepo/storefront/ranking-service/v1"
)

type ResponseGenerator interface {
	GenerateResponse() (*rankingservicev1.RankingResponse, error)
}

package sort

import (
	"bitbucket.org/swigy/ranking-service/api/variant"
)

type VariantSort []*variant.Entity

func (vs VariantSort) Len() int { return len(vs) }
func (vs VariantSort) Less(i, j int) bool {
	ei, ej := vs[i], vs[j]
	if ei == nil && ej == nil {
		return false
	}
	if ei != nil && ej == nil {
		return false
	}
	if ei == nil && ej != nil {
		return true
	}

	resp := compareOrderabilityLt(ei, ej)
	if resp != nil {
		return *resp
	}

	resp = compareDiscountPercentLt(ei, ej)
	if resp != nil {
		return *resp
	}

	resp = compareGrammageLt(ei, ej)
	if resp != nil {
		return *resp
	}

	return false
}

func (vs VariantSort) Swap(i, j int) { vs[i], vs[j] = vs[j], vs[i] }

func compareGrammageLt(ei *variant.Entity, ej *variant.Entity) *bool {
	if ei.WeightValue != ej.WeightValue {
		eval := ei.WeightValue < ej.WeightValue
		return &eval
	}
	return nil
}
func compareDiscountPercentLt(ei *variant.Entity, ej *variant.Entity) *bool {

	eiDiscount := ei.DiscountPercent
	ejDiscount := ej.DiscountPercent

	if eiDiscount != ejDiscount {
		eval := eiDiscount < ejDiscount
		return &eval
	}
	return nil
}

// Orderable is given higher preferance than those which are not orderable
func compareOrderabilityLt(ei *variant.Entity, ej *variant.Entity) *bool {
	if ei.Orderability != ej.Orderability {
		eiLtej := false
		if ei.Orderability == true {
			eiLtej = false
		}
		if ej.Orderability == true {
			eiLtej = true
		}
		return &eiLtej
	}
	return nil
}
